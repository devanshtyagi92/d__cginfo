﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="map.aspx.cs" Inherits="map" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv='cache-control' content='no-cache' />
    <meta http-equiv='expires' content='0' />
    <meta http-equiv='pragma' content='no-cache' />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google-signin-client_id" content="283363650905-uq97sal7gb233p1lebdb546610vff8k9.apps.googleusercontent.com" />

    <title>POI BharatMaps</title>

    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&display=swap" rel="stylesheet" />
    <link rel="shortcut icon" type="image/png" href="img/favicon.png" />
    <link rel="stylesheet" href="https://js.arcgis.com/3.33/esri/themes/calcite/dijit/calcite.css" />
    <link rel="stylesheet" href="https://js.arcgis.com/3.33/esri/themes/calcite/esri/esri.css" />
    <link rel="stylesheet" type="text/css" href="js/agsjs/css/agsjs.css" />
    <link rel="stylesheet" href="js/alertifyjs/css/themes/bootstrap.min.css" />
    <link rel="stylesheet" href="js/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="css/style.css" />
    <style>
        #HomeButton {
            position: absolute;
            top: 95px;
            left: 20px;
            z-index: 50;
        }

        .addPointHeading {
            font-size: 1.1rem;
            font-weight: bold;
        }

        .addPointNote {
            color: #d43b00;
            font-size: 0.99rem;
            font-weight: 500;
        }
        .header h3{
            color:white;
        }
        .header button{
            margin-right:0.5rem;
        }
    </style>
    <script type="text/javascript">
        var dojoConfig = {
            paths: { //if you want to host on your own server, download and put in folders then use path like: 
                agsjs: location.pathname.replace(/\/[^/]+$/, '') + '/js/agsjs'
            }
        };
        window.history.forward(1);
        function captcharefresh() {
            var dateStr = new Date();
            document.getElementById("captcha_Code").src = document.getElementById("captcha_Code").src + '?' + dateStr;
            document.getElementById("txt_Code").value = "";
        }

        function captcharefreshreg() {
            var dateStr = new Date();
            document.getElementById("reg_captcha_Code").src = document.getElementById("captcha_Code").src + '?' + dateStr;
            document.getElementById("reg_captcha").value = "";
        }
        window.onload = function () {
            document.getElementById("captcha_Code").src = 'Captcha.aspx?' + new Date();
            document.getElementById("reg_captcha_Code").src = 'Captcha.aspx?' + new Date();
        };
    </script>
</head>
<body class="calcite">
    <form id="registration_form" runat="server">
    <!-- loginmodal -->
    <div id="login-modal" class="modal-window">
        <div style="border-bottom: 15px solid #7a8d11;">
            <a href="#" id="cls_signup1" title="Close" class="modal-close">X</a>
            <h1>LOGIN</h1>
            <div class="form__group">
                <label class="form__label" for="uname">UserName</label>
                <input type="text" class="form__select" id="uname" name="username" runat="server"/>
            </div>
            <div class="form__group">
                <label class="form__label" for="pwd">Password</label>
                <input type="password" class="form__select" id="pwd" name="password" runat="server"/>
            </div>
            <div class="form__group">
                <label class="form__label" for="cap_ID1">Enter Captcha</label>
                <div class="captcha__inputs" id="cap_ID1">
                    <img src="" id="captcha_Code" width="60px" height="26px" style="border: 1px solid #CBCBCB; margin-bottom: 3px;" />
                    <input type="text" class="form__select" id="txt_Code" name="password" runat="server"/>
                    <button class="btn"><img src="img/refresh.png" onclick="captcharefresh()" width="25px" height="22px" style="background-color: white; border: 1px solid #CBCBCB;" /></button>
                </div>
            </div>
            <div class="form__group">
                 <asp:Button class="btn btn--green" ID="signin_btn" OnClick="Btn_signin_Click" runat="server"   style="cursor:pointer;background: #4caf50;" Text="Log In"/>
               <%-- <button class="btn btn--green" style="margin-bottom: 1.5rem">Sign in</button>--%>
                <button class="btn" id="login_cncl" onclick="document.getElementById('id01').style.display='none'" style="margin-bottom: 1.5rem">Clear</button>
            </div>
            <hr />
            <div class="form__group">
                <div class="signin">
                    <span class="signin__text">Sign in by </span>
                    <span class="signin__btn" onclick="loginFBMethod();">
                        <img src="img/facebook.png" style="height: 30px" alt="" /></span>
                    <span class="signin__btn">
                        <img src="img/google.png" style="height: 30px" alt="" /><div class="g-signin2" data-onsuccess="onSignIn"></div>
                    </span>
                </div>
            </div>
            <div class="forgot__password">
                <span onclick="alert('Please Contact to Administrator');">Forgot Password ?</span>
            </div>

            <div class="forgot__password">
                <a href="#register-modal">Register</a>
            </div>

        </div>
    </div>
    <!-- loginmodal -->
    <!-- Register modal -->
    
        <div id="register-modal" class="modal-window">

            <div style="border-bottom: 15px solid #7a8d11;">
                <a href="#" id="cls_register" title="Close" class="modal-close">X</a>
                <h1>Registration</h1>
                <div class="form__group">
                    <label class="form__label" for="username">UserName</label>
                    <input type="text" class="form__select" id="reg_fname" name="username" runat="server" />
                </div>
                <div class="form__group">
                    <label class="form__label" for="reg_password">Password</label>
                    <input type="password" class="form__select" id="reg_password" name="password" runat="server" />
                </div>
                <div class="form__group">
                    <label class="form__label" for="reg_repassword">Re-Enter Password</label>
                    <input type="password" class="form__select" id="reg_repassword" name="reg_repassword" runat="server" />
                </div>
                <div class="form__group">
                    <label class="form__label" for="mobile">Mobile</label>
                    <input type="text" class="form__select" id="mobile" name="mobile" runat="server" />
                </div>
                <div class="form__group">
                    <label class="form__label" for="email_id">Email Id</label>
                    <input type="text" class="form__select" id="email_id" name="email_id" runat="server" />
                </div>
                <div class="form__group">
                    <label class="form__label" for="securityQ">Select Your Security Question?</label>
                    <select class="form-control form__select" runat="server" id="securityQ" name="securityQ">
                        <option value="select">Choose your Question?</option>
                        <option value="01">What is your favorite sport?</option>
                        <option value="02">What is your favorite childhood place?</option>
                        <option value="03">What is your birth place?</option>
                        <option value="04">What is your favorite website?</option>
                        <option value="05">Who is your favorite teacher?</option>
                        <option value="06">What is your father's favorite book?</option>
                        <option value="07">What is your mother's maiden name?</option>
                    </select>
                </div>
                <div class="form__group">
                    <label class="form__label" for="securityA">Security Question Answer</label>
                    <input type="text" class="form__select" id="securityA" name="securityA" runat="server" />
                </div>
                <div class="form__group">
                    <label class="form__label" for="password">Enter Captcha</label>
                    <div class="captcha__inputs">
                        <img src="" id="reg_captcha_Code" width="60px" height="26px" style="border: 1px solid #CBCBCB; margin-bottom: 3px;" />

                        <input type="text" class="form__select" id="reg_captcha" name="reg_captcha" runat="server" />
                        <button class="btn">
                            <img src="img/refresh.png" onclick="captcharefreshreg()" width="25px" height="22px" style="background-color: white; border: 1px solid #CBCBCB;" /></button>
                    </div>
                </div>
                <div class="form__group">
                    <asp:Button class="btn btn--green" ID="signupBtn" Style="margin-bottom: 1.5rem" runat="server" Text="Sign Up" OnClick="Btn_signup_Click" />
                    <button class="btn" id="signup_cncl" onclick="regFormClear()" style="margin-bottom: 1.5rem">Clear</button>
                </div>
                <hr />

            </div>

        </div>
    </form>

    <!-- Register modal -->
    <!-- feedback modal -->
    <div id="feedback-modal" class="modal-window">
        <div style="border-bottom: 15px solid #7a8d11;">
            <a href="#" id="cls_signup" title="Close" class="modal-close">X</a>
            <h1 style="text-transform: uppercase;">
                <svg style="height: 1.5rem; width: 1.5rem; fill: #7a8d11; margin-right: 0.5rem;">
                    <use xlink:href="img/symbol-defs.svg#icon-bubbles4"></use>
                </svg>Feedback</h1>
            <div class="form__group">
                <label class="form__label" for="feedbackname">Name</label>
                <input type="text" class="form__select" id="" name="feedbackname">
            </div>
            <div class="form__group">
                <label class="form__label" for="feedbackemailid">Email ID</label>
                <input type="text" class="form__select" id="" name="feedbackemailid">
            </div>
            <div class="form__group">
                <label class="form__label" for="feedbackmobilenumber">Mobile Number</label>
                <input type="text" class="form__select" id="" name="feedbackmobilenumber">
            </div>
            <div class="form__group">
                <label class="form__label" for="feedbackissuetype">Issue Type</label>
                <input type="text" class="form__select" id="" name="feedbackissuetype">
            </div>
            <div class="form__group">
                <label class="form__label" for="feedbacksuggestion">Suggestions</label>
                <!-- <input type="text" class="form__select" id="" name="feedbacksuggestion"> -->
                <textarea id="" class="form__select" name="w3review" rows="4" cols="50" style="height: 4rem;"></textarea>
            </div>

            <div class="form__group">
                <button class="btn btn--green" style="margin-bottom: 1.5rem">Submit</button>
            </div>

        </div>
    </div>
    <!-- feedback modal -->
    <div id="map">
    </div>
    <div id="HomeButton"></div>
    <div id="bodyloadimg" style="z-index: 100000; position: absolute; top: 50%; left: 50%; margin-left: -40px; margin-top: -40px;">
        <img src="img/loading.gif" alt='loading..' height="80px" width="80px" />
    </div>
    <section class="navigation">
        <input type="checkbox" class="navigation__checkbox" id="navi-toggle" />
        <label for="navi-toggle" class="navigation__button">
            <span class="navigation__icon">&nbsp;</span>
        </label>
        <div class="navigation__tool">
            <div class="navigation__tool-head">
                <picture class="navigation__tool-head-logo">
                        <source srcset="img/cginfosmallLogoB.png " media="(max-width:37.5em)">
                        <img srcset="img/cginfosmallLogoB.png" alt="Full logo">   
                    </picture>
                <span class="navigation__tool-head-version">0.0.1</span>
            </div>

            <div class="navigation__login">
                <a href="#login-modal">LOGIN</a>
            </div>

            <div class="navigation__tool-panes">
                <ul class="tabs">

                    <li class="tab">
                        <input type="radio" name="tabs" checked="checked" id="tab1" />
                        <label for="tab1">
                            <svg class="tab-icon">
                                <use xlink:href="img/symbol-defs.svg#icon-location11"></use>
                            </svg>
                        </label>
                        <div id="tab-content1" class="content">
                            <div class="form">
                                <div class=" u-margin-bottom-medium">
                                    <h2 class="heading-tertiary">Drill Down Area of Interest</h2>
                                </div>
                                <div class="form__group">
                                </div>
                                <div class="form__group">
                                    <label class="form__label" for="state">Select State</label>
                                    <select class="form__select" name="state" id="statedd"></select>
                                </div>
                                <div class="form__group">
                                    <label class="form__label" for="district">Select District</label>
                                    <select class="form__select" name="district" id="districtdd"></select>
                                </div>
                                <div class="form__group">
                                    <input title="Enter Village name" placeholder="Village name.." type="search" id="txt_srch" style="display: none; width: 30vh; height: 3vh; position: absolute; top: 12%; right: 23%; z-index: 999; color: black;" />
                                    <h5 id="wel_txt" style="position: absolute; top: 11%; font-weight: bold; right: 1%; z-index: 999; display: none;"></h5>
                                </div>
                                <div class="form-u-margin-bottom-medium">
                                </div>
                            </div>
                            <div id="legendDiv"></div>
                        </div>
                    </li>

                    <li class="tab">
                        <input type="radio" name="tabs" id="tab2" />
                        <label for="tab2">
                            <svg class="tab-icon">
                                <use xlink:href="img/symbol-defs.svg#icon-map111"></use>
                            </svg>
                        </label>
                        <div id="tab-content2" class="content">
                            <div id="basemapGallery"></div>
                        </div>
                    </li>

                    <%--*************************************--%>
                    <li class="tab">
                        <input type="radio" name="tabs" id="tab3" />
                        <label for="tab3">
                            <svg class="tab-icon">
                                <use xlink:href="img/symbol-defs.svg#icon-compass2"></use>
                            </svg>
                        </label>
                        <div id="tab-content3" class="content">
                            <%-- <div id="tocDiv">
                                <div id="AddPointInfo" style="height: 70%;">
                                    <div class="InfoSpan" style="margin: auto;">
                                        <span>Please input the following information</span>
                                    </div>
                                    <div id="AddPointContent">

                                        <caption style="font-weight: bold; font-family: sans-serif; font-size: 10px; padding: 5px; color: red;">
                                            <span>Note: The point can be selected from the map. Please fill all the fields and select an image                  before submitting the point.</span><br />
                                            <span id="tytxt" style="font-size: 2.5vh;"></span>
                                        </caption>

                                        <input type="button" id="btnAddPntFrmMap" value="Add Point From Map" style="cursor: pointer; float: left; background-color: #3A5894; color: white; margin: 5px 0px; padding: 5px; width: 100%;" />

                                        <span>Latitude<b style="color: red">✱</b></span>
                                        <input type="number" step="any" id="LatitudeInput" />

                                        <span>Longitude<b style="color: red">✱</b></span>
                                        <input type="number" step="any" id="LongitudeInput" />

                                        <span>Facility Type <b style="color: red">✱</b></span>
                                        <select id="selectorInput"></select>

                                        <span>Sub Facility Type <b style="color: red">✱</b></span>
                                        <select id="selectorInput2" style="width: 192px;"></select>

                                        <span>Facility Name<b style="color: red">✱</b></span>
                                        <input type="text" id="NameInput" />

                                        <span>Facility Address</span>
                                        <input type="text" id="addressInput" runat="server" />

                                        <span>Remarks</span>
                                        <input type="text" id="desc" runat="server" />

                                        <span>Contact Number</span>
                                        <input type="text" id="mobileNoInput" runat="server" maxlength="10" />

                                        <div style="display: none;"><span>Contact Email ID</span><input type="email" id="emailIDInput" /></div>

                                        <span>Picture</span>
                                        <input id="AddPointFileUpload" type='file' style='background-color: #fff;' />


                                        <div id="img_pre" style="display: none;">
                                            <a href="#" onclick="imagepre()">Preview the Image</a>
                                        </div>

                                        <div class="InfoSpan" style="width: 90%; height: 5%; margin: auto;">
                                            <input type="button" id="btnBacktoGrid" value="Close" onclick="_initiateBackFn();" style="cursor: pointer; float: left; background-color: #3A5894; color: white; margin: auto; padding: 5px; width: 49%;" />
                                        </div>
                                        <div class="InfoSpan" style="width: 100%;">
                                            <span id="AddPointMsg" style="color: red; font-weight: bold; font-size: 10px;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div id="ImagePreview_AddPoint" style="height: 30%; display: none;">
                                    <div class="InfoSpan">
                                        <span style="padding-left: 5%;">Image Preview</span>
                                    </div>
                                    <div id="ImageContainer_AddPoint"></div>
                                </div>
                                <div class="container-fluid" id="pnt_info" style="float: left; margin-left: 22vh; margin-top: 4vh; display: none;">
                                    <h3>Selected Point Information </h3>
                                    <h4 id="stnm"></h4>
                                    <h4 id="dtnm"></h4>
                                    <h4 id="vilnm"></h4>
                                </div>
                            </div>--%>
                        </div>
                    </li>

                    <%--*********************************************************--%>

                    <li class="tab">
                        <input type="radio" name="tabs" id="tab4" />
                        <label for="tab4">
                            <svg class="tab-icon">
                                <use xlink:href="img/symbol-defs.svg#icon-database"></use>
                            </svg>
                        </label>
                        <div id="tab-content4" class="content ">
                            <div id="mainWindow" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props="design:'headline',gutters:false"
                                style="width: 100%; height: 100%;">
                                <div id="map1" data-dojo-type="dijit/layout/ContentPane" data-dojo-props="region:'center'">
                                    <div>
                                        <div id="titlePane" data-dojo-type="dijit/TitlePane" data-dojo-props="title:'Measurement', closable:false">
                                            <div id="measurementDiv"></div>
                                            <span style="font-size: smaller; padding: 5px 5px;">Press <b>CTRL</b> to enable snapping.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="navigation__tool-footer">
                <svg class="navigation__tool-footer-icon">
                    <use xlink:href="img/symbol-defs.svg#icon-bubbles4"></use>
                </svg>
                <a href="#feedback-modal" class="feedback">Feedback</a>
            </div>

        </div>
    </section>

    <section class="righticons">
        <a href="#" class="righticons__tools" id="Home">
            <svg class="righticons__tools-icon">
                <use xlink:href="img/symbol-defs.svg#icon-map11"></use>
            </svg>
        </a>

        <a href="#" class="righticons__tools" id="LocateButton">
            <svg class="righticons__tools-icon">
                <use xlink:href="img/symbol-defs.svg#icon-gps_fixed"></use>
            </svg>
        </a>

        <a href="#" class="righticons__tools">
            <svg class="righticons__tools-icon">
                <use xlink:href="img/symbol-defs.svg#icon-zoom_out_map"></use>
            </svg>
        </a>

        <a href="#" class="righticons__tools">
            <svg class="righticons__tools-icon">
                <use xlink:href="img/symbol-defs.svg#icon-location_off"></use>
            </svg>
        </a>

        <a href="#" class="righticons__tools">
            <svg class="righticons__tools-icon">
                <use xlink:href="img/symbol-defs.svg#icon-location_city"></use>
            </svg>
        </a>

        <a href="#" class="righticons__tools">
            <svg class="righticons__tools-icon">
                <use xlink:href="img/symbol-defs.svg#icon-location_history"></use>
            </svg>
        </a>

        <a href="#" class="righticons__tools">
            <svg class="righticons__tools-icon">
                <use xlink:href="img/symbol-defs.svg#icon-location-arrow-outline"></use>
            </svg>
        </a>


    </section>

    <!-- <button class="addpoint">+</button> -->
    <div class="floating-chat">
        <!-- <i class="fa fa-comments" aria-hidden="true"></i> -->
        <!-- <button class="addpoint">+</button> -->
        <svg class="floating-chat-icon">
            <use xlink:href="img/symbol-defs.svg#icon-add_location"></use>
        </svg>
        <div class="chat">
            <div class="header">
                <span class="title">
                    <h3>Enter Details to Add Points.</h3>
                </span>
                <button>
                    <svg class="floating-chat-closeicon">
                        <use xlink:href="img/symbol-defs.svg#icon-cross"></use>
                    </svg>
                </button>

            </div>

            <div class="messages">
                <div id="tocDiv">
                    <div id="AddPointInfo" style="height: 70%;">
                       <%-- <div class="InfoSpan" style="margin: auto;">
                            <span>Please input the following information</span>
                        </div>--%>
                        <div id="AddPointContent">

                            <div class="chat-note">
                                <span style="color: red; font-weight: 700; font-size: 1rem;">Note:</span>
                                <p style="color: red; font-weight: 700; font-size: 1rem;">The point can be selected from the map. Please fill all the fields and select an image before submitting the point.</p>
                                <span id="tytxt" style="font-size: 2.5vh;"></span>
                            </div>
                            <button class="chat-btn" id="btnAddPntFrmMap" style="margin: 0.5rem auto">Add point from Map</button><hr />

                            <div class="chat__form">
                                <label class="chat__form-label form__label" for="latitude">Latitude<b style="color: red">✱</b></label>
                                <input type="number" class="chat__form-select form__select" step="any" id="LatitudeInput" name="latitude" />
                            </div>

                            <div class="chat__form">
                                <label class="chat__form-label form__label" for="longitude">Longitude<b style="color: red">✱</b></label>
                                <input type="number" step="any" id="LongitudeInput" class="chat__form-select form__select" name="longitude" />
                            </div>

                            <div class="chat__form">
                                <label class="chat__form-label form__label" for="facilitytype">Facility type<b style="color: red">✱</b></label>
                                <select class="form__select" name="facilitytype" id="selectorInput"></select>
                            </div>

                            <div class="chat__form">
                                <label class="chat__form-label form__label" for="subfacilitytype">Sub Facility Type<b style="color: red">✱</b></label>
                                <select class="form__select" name="subfacilitytype" id="selectorInput2"></select>
                            </div>

                            <div class="chat__form">
                                <label class="chat__form-label form__label" for="facilityname">Facility Name<b style="color: red">✱</b></label>
                                <input type="text" class="chat__form-select form__select" id="NameInput" name="facilityname" />
                            </div>

                            <div class="chat__form">
                                <label class="chat__form-label form__label" for="facilityaddress">Facility Address</label>
                                <input type="text" class="chat__form-select form__select" id="addressInput" name="facilityaddress" runat="server"/>
                            </div>

                            <div class="chat__form">
                                <label class="chat__form-label form__label" for="remarks">Remarks</label>
                                <input type="text" class="chat__form-select form__select" id="desc" name="remarks" runat="server"/>
                            </div>

                            <div class="chat__form">
                                <label class="chat__form-label form__label" for="contactnumber">Contact Number</label>
                                <input type="text" class="chat__form-select form__select" id="mobileNoInput" name="contactnumber" runat="server" maxlength="10"/>
                            </div>

                            <div style="display: none;" class="chat__form">
                                <span>Contact Email ID</span>
                                <input type="email" id="emailIDInput" />
                            </div>

                            <div class="chat__form">
                                <label class="chat__form-label form__label" for="contactnumber">Picture</label>
                                <input id="AddPointFileUpload" type='file' class="chat__form-select form__select" name="contactnumber" runat="server" maxlength="10"/>
                            </div>


                            <div id="img_pre" style="display: none;">
                                <a href="#" onclick="imagepre()">Preview the Image</a>
                            </div>

                             <button class="chat-btn" id="btnBacktoGrid" style="margin: 0.5rem auto" onclick="_initiateBackFn();" >Add point from Map</button><hr />
                         
                            <div class="InfoSpan" style="width: 100%;">
                                <span id="AddPointMsg" style="color: red; font-weight: bold; font-size: 10px;"></span>
                            </div>
                        </div>
                    </div>
                    <div id="ImagePreview_AddPoint" style="height: 30%; display: none;">
                        <div class="InfoSpan">
                            <span style="padding-left: 5%;">Image Preview</span>
                        </div>
                        <div id="ImageContainer_AddPoint"></div>
                    </div>
                    <div class="" id="pnt_info" style=" display: none;">
                        <h3>Selected Point Information </h3>
                        <h4 id="stnm"></h4>
                        <h4 id="dtnm"></h4>
                        <h4 id="vilnm"></h4>
                    </div>
                </div>

            </div>


            <div class="footer" style="display: none;">
                <div class="text-box" contenteditable="true" disabled="true"></div>
                <button id="sendMessage">send</button>
            </div>
        </div>
    </div>







</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/alertifyjs/alertify.js"></script>
<script src="js/sha256.js"></script>

<script src="https://js.arcgis.com/3.33/"></script>
<script src="js/config.js"></script>
<script src="js/map.js"></script>
<script>
    function loginFBMethod() {
        FB.login(function (response) {
            // handle the response
            if (response.status == "connected") {
                FB.api('/me?fields=name,email,id,picture.width(150).height(150)', function (response) {
                    statusChangeCallback(response, 0);
                });
            }
            else {
                console.log("something went wrong.");
            }
        }, { scope: 'public_profile,email' });
    }

    function onSignIn(googleUser) {
        var profile = googleUser.getBasicProfile();
        statusChangeCallback(profile, 1);
        //console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
        //console.log('Name: ' + profile.getName());
        //console.log('Image URL: ' + profile.getImageUrl());
        //console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
    }
</script>
<script>
    function validateLoginForm() {
        var user_name = $("#uname").val();
        var password = $("#pwd").val();
        var Reg_captcha = $("#txt_Code").val();
        if (user_name == "") {
            alertify.alert("warning", "Please enter UserName");
            $("#user_name").focus();
            return false;
        }
        if (password == "") {
            alertify.alert("warning", "Please enter Password");
            $("#pwd").focus();
            return false;
        }
        if (Reg_captcha == "") {
            alertify.alert("warning", "Please enter Captcha");
            $("#txt_Code").focus();
            return false;
        }
        return true;
    }
    function validateRegForm() {
        var user_name = $("#reg_fname").val();
        var password = $("#reg_password").val();
        var reg_password = $("#reg_repassword").val();
        var mobile_number = $("#mobile").val();
        var email_id = $("#email_id").val();
        var securityQ = $('#securityQ').val();
        var securityA = $("#securityA").val();
        var Reg_captcha = $("#reg_captcha").val();
        if (user_name == "") {
            alertify.alert("warning", "Please enter UserName");
            $("#reg_fname").focus();
            return false;
        }
        if (password == "") {
            alertify.alert("warning", "Please enter Password");
            $("#reg_password").focus();
            return false;
        }
        if (reg_password == "") {
            alertify.alert("warning", "Please enter Re-Enter Password");
            $("#reg_password").focus();
            return false;
        }
        if (password != reg_password) {
            alertify.alert("warning", "Password is not matching with Re-Enter Password,Please enter again");
            $("#reg_password").val("");
            $("#reg_repassword").val("");
            return false;
        }
        if (mobile_number == "") {
            alertify.alert("warning", "Please enter Mobile");
            $("#mobile").focus();
            return false;
        }
        if (isNaN(mobile_number) || mobile_number.indexOf(" ") != -1) {
            alertify.alert("warning", "Enter numeric value");
            $("#mobile").val("");
            return false;
        }
        if (mobile_number.length > 10) {
            alert("enter 10 characters");
            return false;
        }
        if (mobile_number.charAt(0) != "9" && mobile_number.charAt(0) != "8" && mobile_number.charAt(0) != "7" && mobile_number.charAt(0) != "6") {
            alert("it should start with 9/8/7/6");
            return false
        }
        if (email_id == "") {
            alertify.alert("warning", "Please enter Email Id");
            $("#email_id").focus();
            return false;
        }
        if (!IsEmail(email_id)) {
            alertify.alert("warning", "Please enter Valid Email Id");
            $("#email_id").focus().val("");
            return false;
        }
        if (securityQ == "select") {
            alertify.alert("warning", "Please Select Your Security Question");
            return false;
        }
        if (securityA == "") {
            alertify.alert("warning", "Please Enter Security Question Answer");
            $("#securityA").focus();
            return false;
        }
        if (Reg_captcha == "") {
            alertify.alert("warning", "Please enter Captcha");
            $("#reg_captcha").focus();
            return false;
        }
        return true;
    }

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!regex.test(email)) {
            return false;
        } else {
            return true;
        }
    }

    function regFormClear() {
        $("#reg_fname").val("");
        $("#reg_password").val("");
        $("#reg_repassword").val("");
        $("#mobile").val("");
        $("#email_id").val("");
        $('#securityQ').prop('selectedIndex', 0);
        $("#securityA").val("");
        $("#reg_captcha").val("");
    }

    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function allowAlphaNumericSpace(thisInput) {
        thisInput.value = thisInput.value.split(/[^a-zA-Z0-9_@ ]/).join('');
    }

    function allowNumeric(thisInput) {
        thisInput.value = thisInput.value.split(/[^0-9]/).join('');
    }

    function Reg_form_sha256encode(random, id) {
        var value = document.getElementById(id).value;
        var shaencry = sha256_digest(value) + random;
        document.getElementById(id).value = sha256_digest(shaencry);
    }

    function sha256encode(random, id) {
        var value = document.getElementById(id).value;
        document.getElementById(id).value = sha256_digest(value);
    }
</script>
<script>
    var element = $('.floating-chat');
    var myStorage = localStorage;

    if (!myStorage.getItem('chatID')) {
        myStorage.setItem('chatID', createUUID());
    }

    setTimeout(function () {
        element.addClass('enter');
    }, 1000);

    element.click(openElement);

    function openElement() {
        var messages = element.find('.messages');
        var textInput = element.find('.text-box');
        element.find('>i').hide();
        element.addClass('expand');
        element.find('.chat').addClass('enter');
        var strLength = textInput.val().length * 2;
        textInput.keydown(onMetaAndEnter).prop("disabled", false).focus();
        element.off('click', openElement);
        element.find('.header button').click(closeElement);
        element.find('#sendMessage').click(sendNewMessage);
        messages.scrollTop(messages.prop("scrollHeight"));
    }

    function closeElement() {
        element.find('.chat').removeClass('enter').hide();
        element.find('>i').show();
        element.removeClass('expand');
        element.find('.header button').off('click', closeElement);
        element.find('#sendMessage').off('click', sendNewMessage);
        element.find('.text-box').off('keydown', onMetaAndEnter).prop("disabled", true).blur();
        setTimeout(function () {
            element.find('.chat').removeClass('enter').show()
            element.click(openElement);
        }, 500);
    }

    function createUUID() {
        // http://www.ietf.org/rfc/rfc4122.txt
        var s = [];
        var hexDigits = "0123456789abcdef";
        for (var i = 0; i < 36; i++) {
            s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
        }
        s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
        s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
        s[8] = s[13] = s[18] = s[23] = "-";

        var uuid = s.join("");
        return uuid;
    }

    function sendNewMessage() {
        var userInput = $('.text-box');
        var newMessage = userInput.html().replace(/\<div\>|\<br.*?\>/ig, '\n').replace(/\<\/div\>/g, '').trim().replace(/\n/g, '<br>');

        if (!newMessage) return;

        var messagesContainer = $('.messages');

        messagesContainer.append([
            '<li class="self">',
            newMessage,
            '</li>'
        ].join(''));

        // clean out old message
        userInput.html('');
        // focus on input
        userInput.focus();

        messagesContainer.finish().animate({
            scrollTop: messagesContainer.prop("scrollHeight")
        }, 250);
    }

    function onMetaAndEnter(event) {
        if ((event.metaKey || event.ctrlKey) && event.keyCode == 13) {
            sendNewMessage();
        }
    }
</script>

</html>
