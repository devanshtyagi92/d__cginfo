﻿var map;
require(["esri/map", "esri/geometry/Extent", "esri/SpatialReference", "esri/symbols/SimpleMarkerSymbol", "esri/symbols/PictureMarkerSymbol", "esri/InfoTemplate",
    "esri/symbols/SimpleLineSymbol", "esri/Color", "esri/geometry/Point", "esri/graphic", "esri/layers/GraphicsLayer", "esri/symbols/SimpleFillSymbol", "esri/dijit/BasemapGallery", "esri/dijit/BasemapLayer", "esri/dijit/Basemap", "esri/dijit/Measurement",
    "esri/geometry/webMercatorUtils", "esri/layers/ArcGISDynamicMapServiceLayer", "esri/tasks/query", "esri/tasks/QueryTask", "esri/layers/FeatureLayer",
    "esri/toolbars/draw", "esri/SnappingManager", "esri/dijit/ElevationProfile", "esri/units", "esri/dijit/Legend", "esri/renderers/UniqueValueRenderer",
    "esri/toolbars/navigation", "dojo/on", "dijit/registry", "dojo/_base/event", "dojo/_base/array", "dojo/dom", "agsjs/dijit/TOC", "dojo/domReady!"],
    function (Map, Extent, SpatialReference, SimpleMarkerSymbol, PictureMarkerSymbol, InfoTemplate, SimpleLineSymbol, Color, Point, Graphic, GraphicsLayer, SimpleFillSymbol, BasemapGallery, BasemapLayer, Basemap, Measurement,
        webMercatorUtils, ArcGISDynamicMapServiceLayer, Query, QueryTask, FeatureLayer, Draw, SnappingManager, ElevationProfile, units, Legend, UniqueValueRenderer,
        Navigation, on, registry, event, arrayUtils, dom, TOC) {
        map = new Map("map", { basemap: "topo", center: [78, 23], zoom: 5 });
        //********** Adding Dynamic layers to Map *********************************************      
        var admin = new ArcGISDynamicMapServiceLayer(vil_service + vil_token);
        map.addLayers([admin]);
        admin.setVisibleLayers([0, 1, 3]);
        var ind_ext = new Extent(66.62, 5.23, 98.87, 38.59, new SpatialReference({ wkid: 4326 }));
        map.setExtent(ind_ext);
        map.on("load", function () {//open  map load function
            states_display();
            Loadbasemaps();
        });//close  map load function
        function Loadbasemaps() {
            var basemaps = [];
            if (basemapsList.length > 0) {
                for (var i = 0; i < basemapsList.length; i++) {
                    var baselayer = new BasemapLayer({
                        url: basemapsList[i].url + basemapsList[i].token
                    });
                    var _basemaps = new Basemap(basemapsList[i].options);
                    _basemaps.layers = [baselayer];
                    basemaps.push(_basemaps);
                }
            }
            basemapgallery = new BasemapGallery({
                showArcGISBasemaps: false,
                basemaps: basemaps,
                map: map
            }, "basemapGallery");
            basemapgallery.on("error", function (msg) {
                console.log("basemap gallery error:  ", msg);
            });
            basemapgallery.startup();
        }
        function states_display() {   // open filling state dropdown
            $('#bodyloadimg').show();
            var queryTask = new QueryTask(stateUrl);
            var query = new Query();
            query.returnGeometry = false;
            query.outFields = ["*"];
            query.where = "1=1";
            query.orderByFields = ["STNAME"];
            queryTask.execute(query, function (features) {
                if (features.features.length > 0) {// to set extent according to state selection  
                    $("#statedd").empty();
                    $("#statedd").append(new Option("Select State", "select"));
                    adddatatodropdown(features.features, "statedd", "STCODE11", "STNAME");
                }
                else {
                    alertify.alert("", "No states found");
                }
            });
            $('#bodyloadimg').hide();
        }//close filling state dropdown
        $("#statedd").change(function () {//open statedd change event
            clearAll();
            selected_state = this.value;
            selected_district = "select";
            if (selected_state == "select") {
                $("#districtdd").empty();
                $("#districtdd").append(new Option("Select District", "select"));
                map.setExtent(admin.fullExtent);
                admin.setDefaultLayerDefinitions();
            }
            else {
                district_display();
                var query_wherecondition = "STCODE11='" + selected_state + "'";//STCODE11='10'  
                //var query_wherecondition = "STCODE11=" + selected_state;//STCODE11=10
                zoom(query_wherecondition, stateUrl);//to set extent according to state selection
            }
        });//close statedd change event
        function district_display() {   // open filling district dropdown
            $('#bodyloadimg').show();
            var queryTask = new QueryTask(districtUrl);//stateurl
            var query = new Query();
            query.returnGeometry = false;
            query.outFields = ["*"];
            query.where = "stcode11='" + selected_state + "'";
            query.orderByFields = ["dtname"];
            queryTask.execute(query, function (features) {
                if (features.features.length > 0) {// to set extent according to state selection  
                    $("#districtdd").empty();
                    $("#districtdd").append(new Option("Select District", "select"));
                    adddatatodropdown(features.features, "districtdd", "dtcode11", "dtname");
                }
                else {
                    alertify.alert("", "No district found");
                }
            });
            $('#bodyloadimg').hide();
        }//close filling district dropdown
        $("#districtdd").change(function () {//open districtdd change event
            clearAll();
            selected_district = this.value;
            if (selected_district == "select") {
                $("#districtdd").empty();
                $("#districtdd").append(new Option("Select District", "select"));
                var query_wherecondition = "STCODE11='" + selected_state + "'";
                zoom(query_wherecondition, stateUrl);//to set extent according to state selection
            }
            else {
                var query_wherecondition = "dtcode11='" + selected_district + "'";
                zoom(query_wherecondition, districtUrl);//to set extent according to district selection
            }
        });//close districtdd change event
        function adddatatodropdown(features, id, fieldid, fieldname) {//open function adddatatodropdown
            for (var i = 0; i < features.length; i++) {
                $("#" + id).append(new Option(features[i].attributes[fieldname], features[i].attributes[fieldid]));
            }
        }//close function adddatatodropdown
        function clearAll() {
            map.graphics.clear();
        }
        function zoom(wherecondition, url) {//open function zoom
            $("#bodyloadimg").show();
            var queryTask = new QueryTask(url);
            var query = new Query();
            query.returnGeometry = true;
            query.outFields = ["*"];
            query.where = wherecondition;
            queryTask.execute(query, function (fset) {
                if (fset.features.length > 0) {
                    map.setExtent(fset.features[0].geometry.getExtent(), true);
                    $("#bodyloadimg").hide();
                } else {
                    alert("No features found for zoom");
                }
            }, function (error) {
                $("#bodyloadimg").hide();
                console.log(error);
            });
        }//close function zoom
        var symbol1 = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 12, new SimpleLineSymbol(SimpleLineSymbol.STYLE_NULL, new Color([19, 10, 291, 0.9]), 1), new Color([19, 10, 291, 1]));
        //var default_marker = new PictureMarkerSymbol({ "angle": 0, "xoffset": 0, "yoffset": 10, "type": "esriPMS", "url": "img/", "contentType": "image/gif", "width": 16, "height": 18 });
        var highlightlineSymbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 215, 0]), 4);
    })