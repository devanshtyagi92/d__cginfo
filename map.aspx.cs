﻿using Npgsql;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;

public partial class map : System.Web.UI.Page
{
    NpgsqlConnection con = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["postgresConnection"].ToString());
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Random rd = new Random();
            string Randm_Number = rd.Next().ToString();
            ViewState["Random"] = Randm_Number;
            signin_btn.Attributes.Add("OnClick", "Reg_form_sha256encode(" + "'" + Randm_Number + "'" + ",'pwd');return validateLoginForm();");
            signupBtn.Attributes.Add("OnClick", "sha256encode(" + "'" + Randm_Number + "'" + ",'reg_password');sha256encode(" + "'" + Randm_Number + "'" + ",'reg_repassword');return validateRegForm();");
            Session["update"] = Server.UrlEncode(System.DateTime.Now.ToString());
        }
    }
    protected override void OnPreRender(EventArgs e)
    {
        ViewState["update"] = Session["update"];
    }

    protected void Btn_signin_Click(object sender, EventArgs e)
    {

        try
        {
            if (uname.Value == "")
            {
                MsgBoxDisplay("Please enter UserName");
                clearLoginFields();
                return;
            }
            else if (pwd.Value == "")
            {
                MsgBoxDisplay("Please enter Password");
                clearLoginFields();
                return;
            }
            else if (txt_Code.Value == "")
            {
                MsgBoxDisplay("Please Enter Captcha");
                clearLoginFields();
                return;
            }
            else if (txt_Code.Value.ToString() != Session["CAPTCHA"].ToString())
            {
                MsgBoxDisplay("Error: Captcha code mismatch!");
                clearLoginFields();
                return;
            }
            else
            {
                Session["CAPTCHA"] = null;
            }
            string user_name = uname.Value;
            string password = pwd.Value;
            if (validateInput(user_name, @"^[A-Za-z0-9_]{6,15}$") && validateInput(password, "^[a-zA-Z0-9_]*$"))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                string ip = getUserIP();
                if (ip != "") ;
                DataTable dt = new DataTable();
                NpgsqlCommand cmd = new NpgsqlCommand("select * from login_user where username=@username", con);
                cmd.Parameters.AddWithValue("@username", user_name);
                NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    string dbpwd = dt.Rows[0]["password"].ToString();
                    string encrdbpwd = sha256encode(dbpwd + ViewState["Random"].ToString());
                    if (encrdbpwd == password)
                    {
                        HttpContext.Current.Session["Login"] = "Successfull";
                        HttpContext.Current.Session["Username"] = user_name;
                        HttpContext.Current.Session["emailID"] = dt.Rows[0]["emailid"].ToString();

                        string query = "insert into audit_trail(userid,clientip,entrydate,event,status) values(@UserID,@ClientIP,@EntryDate,@Event,@ActionType)";
                        NpgsqlCommand cmdd = new NpgsqlCommand(query, con);
                        cmdd.Parameters.AddWithValue("@UserID", user_name);
                        cmdd.Parameters.AddWithValue("@ClientIP", ip);
                        cmdd.Parameters.AddWithValue("@EntryDate", DateTime.Now.ToLongDateString().ToString());
                        cmdd.Parameters.AddWithValue("@Event", "Login");
                        cmdd.Parameters.AddWithValue("@ActionType", "Login Successfull");
                        cmdd.ExecuteNonQuery();
                        NpgsqlCommand update_cmd = new NpgsqlCommand("UPDATE login_user SET flag=@flag WHERE username=@username", con);
                        update_cmd.Parameters.AddWithValue("@flag", "1");
                        update_cmd.Parameters.AddWithValue("@username", user_name);
                        update_cmd.ExecuteNonQuery();
                        string script = string.Format("var logFlag = {0}; var Username = '{1}'; document.getElementById(\"log1\").style.display=\"none\"; document.getElementById(\"log2\").style.display=\"block\";", "true", user_name);
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "<script>" + script + "</script>", false);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Login Successfully')", true);
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "LogUser('" + user_name + "')", true);
                    }
                    else
                    {
                        //No data Wrong User details
                        loginAuditTrail();
                        string script = string.Format("var logFlag = {0}; ", "false");
                        //ScriptManager.RegisterStartupScript(this, GetType(), "msg", "<script>alert('Provide Valid UserName/Password');" + script + "</script>", false);
                        clearLoginfields();
                        MsgBoxDisplay(script+"Provide Valid UserName/Password");
                        return;
                    }
                }
                else
                {
                    //No Data
                    loginAuditTrail();
                    clearLoginfields();
                    MsgBoxDisplay("Provide Valid UserName/Password");
                    return;
                }
            }
        }

        catch (Exception ex)
        {
            con.Close();
            MsgBoxDisplay(ex.Message);
        }

    }
    protected void loginAuditTrail()
    {
        try
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }

            string ip = getUserIP();
            if (ip != "")
            {
                string Pwd = pwd.Value;// getHashSha256(pwd.Value);
                string Username = uname.Value;//sqlinjection(uname.Value);


                string query = "insert into audit_trail(userid,clientip,entrydate,event,status) values(" +
                           "@UserID,@ClientIP,current_timestamp,@Event,@ActionType)";
                NpgsqlCommand cmd = new NpgsqlCommand(query, con);
                cmd.Parameters.AddWithValue("@UserID", Username);
                cmd.Parameters.AddWithValue("@ClientIP", ip);
                cmd.Parameters.AddWithValue("@EntryDate", DateTime.Now.ToLongDateString().ToString());
                //cmd.Parameters.AddWithValue("@InTime", DateTime.Now.ToString());
                cmd.Parameters.AddWithValue("@Event", "Login");
                cmd.Parameters.AddWithValue("@ActionType", "Login Failed");
                int i = cmd.ExecuteNonQuery();

            }
            else
            {
                con.Close();
            }
        }
        catch (Exception ex)
        {
            con.Close();
            MsgBoxDisplay(ex.Message);
        }
    }
    private void clearLoginFields() {
        uname.Value = "";
        pwd.Value = "";
        txt_Code.Value = "";
    }

    protected void Btn_Login_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["update"].ToString() == ViewState["update"].ToString()) // If page not Refreshed 
            {
                Session["update"] = Server.UrlEncode(System.DateTime.Now.ToString());
                if (uname.Value == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "<script>alert('Please Enter User Name');</script>", false);
                    clearLoginfields();
                    return;
                }
                else if (pwd.Value == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "<script>alert('Please Enter Password');</script>", false);
                    clearLoginfields();
                    return;
                }
                else if (txt_Code.Value == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "<script>alert('Please Enter Captcha Code');</script>", false);
                    clearLoginfields();
                    return;
                }
                else if (txt_Code.Value != Session["CAPTCHA"].ToString())
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "<script>alert('Error: Captcha code mismatch!');</script>", false);
                    clearLoginfields();
                    return;
                }
                else
                {

                    Session["CAPTCHA"] = "";
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    string ip = getUserIP();
                    if (ip != "")
                    {
                        string Pwd = pwd.Value;// getHashSha256(pwd.Value);
                        string Username = uname.Value;//sqlinjection(uname.Value);
                        string dbPwd;

                        try
                        {
                            DataSet dt = new DataSet();
                            NpgsqlDataAdapter sda;
                            string strQuery = "SELECT password,emailid FROM login_user WHERE username = @Username";
                            NpgsqlCommand cmd1 = new NpgsqlCommand(strQuery);
                            cmd1.Parameters.AddWithValue("@UserName", Username);
                            sda = new NpgsqlDataAdapter();
                            cmd1.CommandType = CommandType.Text;
                            cmd1.Connection = con;
                            sda.SelectCommand = cmd1;
                            sda.Fill(dt);
                            DataSet dbReader = dt;
                            if (((!object.ReferenceEquals(dbReader, DBNull.Value))))
                            {
                                if (dbReader.Tables[0].Rows.Count > 0)
                                {
                                    dbPwd = dbReader.Tables[0].Rows[0][0].ToString();
                                    if (Pwd == sha256encode(Session["randomSalt"] + dbPwd))
                                    {

                                        #region ToAVIOD SESSION HIJACKING
                                        Request.Cookies.Clear();
                                        Response.Cookies.Add(new HttpCookie("Check", Guid.NewGuid() + Session.SessionID + Guid.NewGuid()));
                                        Session["check"] = Request.Cookies["Check"].Value.ToString();
                                        #endregion
                                        HttpContext.Current.Session["Login"] = "Successfull";
                                        HttpContext.Current.Session["Username"] = Username;
                                        HttpContext.Current.Session["emailID"] = dbReader.Tables[0].Rows[0][1].ToString();

                                        string query = "insert into audit_trail(userid,clientip,entrydate,timestamp,event,status) values(@UserID,@ClientIP,@EntryDate,@InTime,@Event,@ActionType)";
                                        NpgsqlCommand cmd = new NpgsqlCommand(query, con);
                                        cmd.Parameters.AddWithValue("@UserID", Username);
                                        cmd.Parameters.AddWithValue("@ClientIP", ip);
                                        cmd.Parameters.AddWithValue("@EntryDate", DateTime.Now.ToLongDateString().ToString());
                                        cmd.Parameters.AddWithValue("@InTime", DateTime.Now.ToString());
                                        cmd.Parameters.AddWithValue("@Event", "Login");
                                        cmd.Parameters.AddWithValue("@ActionType", "Login Successfull");
                                        int i = cmd.ExecuteNonQuery();
                                        NpgsqlCommand update_cmd = new NpgsqlCommand("UPDATE login_user SET flag=@flag WHERE username=@username", con);
                                        update_cmd.Parameters.AddWithValue("@flag", "1");
                                        update_cmd.Parameters.AddWithValue("@username", Username);
                                        update_cmd.ExecuteNonQuery();
                                        string script = string.Format("var logFlag = {0}; var Username = '{1}'; document.getElementById(\"log1\").style.display=\"none\"; document.getElementById(\"log2\").style.display=\"block\";", "true", Username);
                                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "<script>" + script + "</script>", false);
                                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Login Successfully')", true);
                                        Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "LogUser('" + Username + "')", true);

                                    }
                                    else
                                    {
                                        //string query = "insert into audit_trail(userid,clientip,entrydate,timestamp,event,status) values(" +
                                        //             "@UserID,@ClientIP,@EntryDate,@InTime,@Event,@ActionType)";
                                        //NpgsqlCommand cmd = new NpgsqlCommand(query, con);
                                        //cmd.Parameters.AddWithValue("@UserID", Username);
                                        //cmd.Parameters.AddWithValue("@ClientIP", ip);
                                        //cmd.Parameters.AddWithValue("@EntryDate", DateTime.Now.ToLongDateString().ToString());
                                        //cmd.Parameters.AddWithValue("@InTime", DateTime.Now.ToString());
                                        //cmd.Parameters.AddWithValue("@Event", "Login");
                                        //cmd.Parameters.AddWithValue("@ActionType", "Login Failed");
                                        //int i = cmd.ExecuteNonQuery();
                                        loginAuditTrail();
                                        string script = string.Format("var logFlag = {0}; ", "false");
                                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "<script>alert('Provide Valid UserName/Password');" + script + "</script>", false);
                                        clearLoginfields();
                                        return;
                                    }

                                }
                                else
                                {
                                    //string query = "insert into audit_trail(userid,clientip,entrydate,timestamp,event,status) values(" +
                                    //                 "@UserID,@ClientIP,@EntryDate,@InTime,@Event,@ActionType)";
                                    //NpgsqlCommand cmd = new NpgsqlCommand(query, con);
                                    //cmd.Parameters.AddWithValue("@UserID", Username);
                                    //cmd.Parameters.AddWithValue("@ClientIP", ip);
                                    //cmd.Parameters.AddWithValue("@EntryDate", DateTime.Now.ToLongDateString().ToString());
                                    //cmd.Parameters.AddWithValue("@InTime", DateTime.Now.ToString());
                                    //cmd.Parameters.AddWithValue("@Event", "Login");
                                    //cmd.Parameters.AddWithValue("@ActionType", "Login Failed");
                                    //int i = cmd.ExecuteNonQuery();
                                    loginAuditTrail();
                                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "<script>alert('Please Provide Valid UserName and Password');</script>", false);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.ToString();
                            return;
                        }
                        finally
                        {
                            con.Close();
                            con.Dispose();
                            clearLoginfields();
                        }
                    }
                    return;
                }
            }
            else
            {
                string logStatus = (string)HttpContext.Current.Session["Login"];
                Session["update"] = Server.UrlEncode(System.DateTime.Now.ToString());
                if (!string.IsNullOrEmpty(logStatus))
                {
                    //if ("Successfull".Equals(logStatus))
                    //{
                    //    //log_Flag = "true";
                    //    //string script = string.Format("var logFlag = {0}; var Username = '{1}'; var imgUrl = '{2}'; document.getElementById(\"log1\").style.display=\"none\"; document.getElementById(\"log2\").style.display=\"block\";", log_Flag, (string)Session["Username"], (string)Session["imgUrl"]);
                    //    //ScriptManager.RegisterStartupScript(this, GetType(), "msg", "<script>" + script + "</script>", false);
                    //}
                    //else
                    //{
                    //    //log_Flag = "false";
                    //    //string script = string.Format("var logFlag = {0}; var Username = '{1}'; document.getElementById(\"log1\").style.display=\"block\"; document.getElementById(\"log2\").style.display=\"none\";", log_Flag, (string)Session["Username"]);
                    //    //ScriptManager.RegisterStartupScript(this, GetType(), "msg", "<script>" + script + "</script>", false);
                    //}
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "<script>alert('" + ex.ToString() + "');</script>", false);
            Response.Redirect("map.aspx", true);
        }
    }

   

    protected void Btn_signup_Click(object sender, EventArgs e)
    {
        try
        {
            if (reg_fname.Value == "")
            {
                MsgBoxDisplay("Please enter UserName");
                clearRegistrationFields();
                return;
            }
            else if (reg_password.Value == "")
            {
                MsgBoxDisplay("Please enter Password");
                clearRegistrationFields();
                return;
            }
            else if (reg_repassword.Value == "")
            {
                MsgBoxDisplay("Please enter Re-Enter Password");
                clearRegistrationFields();
                return;
            }
            else if (mobile.Value == "")
            {
                MsgBoxDisplay("Please enter Mobile");
                clearRegistrationFields();
                return;
            }
            else if (email_id.Value == "")
            {
                MsgBoxDisplay("Please enter Email Id");
                clearRegistrationFields();
                return;
            }
            else if (securityQ.Value == "select" || securityQ.Value == "")
            {
                MsgBoxDisplay("Please Select Security Question");
                clearRegistrationFields();
                return;
            }
            else if (securityA.Value == "")
            {
                MsgBoxDisplay("Please Enter Security Question Answer");
                clearRegistrationFields();
                return;
            }
            else if (reg_captcha.Value == "")
            {
                MsgBoxDisplay("Please Enter Captcha");
                clearRegistrationFields();
                return;
            }
            else if (reg_captcha.Value.ToString() != Session["CAPTCHA"].ToString())
            {
                MsgBoxDisplay("Error: Captcha code mismatch!");
                clearRegistrationFields();
                return;
            }
            else
            {
                Session["CAPTCHA"] = null;
            }
            string user_name = reg_fname.Value;
            string password = reg_password.Value;
            string confirmPassword = reg_repassword.Value;
            string mobile_number = mobile.Value;
            string email_address = email_id.Value;
            string securityQuestion = securityQ.Value;
            string securityAnswer = securityA.Value;
            if (validateInput(user_name,
                @"^[A-Za-z0-9_]{6,15}$") && validateInput(password,
                "^[a-zA-Z0-9_]*$") && validateInput(mobile_number,
                @"^[0-9]{10,12}$") && validateInput(email_address,
                "^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$") && validateInput(securityQuestion,
                @"^[0-9]{1,2}$") && validateInput(securityAnswer, "^[a-zA-Z0-9_]*$"))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                NpgsqlCommand cmd = new NpgsqlCommand("select COUNT(*) from login_user where username=@username and emailid=@emailid", con);
                cmd.Parameters.AddWithValue("@username", user_name);
                cmd.Parameters.AddWithValue("@emailid", email_address);
                Int64 UserExist = (Int64)cmd.ExecuteScalar();
                if (UserExist == 0)
                {
                    cmd = new NpgsqlCommand("INSERT INTO login_user(username, password, mobileno, emailid, sqnumber, sqanswer, signupdate, changepwdate) VALUES(@username, @password, @mobileno,@emailid,@sqnumber, @sqanswer, current_timestamp,current_timestamp); ", con);
                    cmd.Parameters.AddWithValue("@username", user_name);
                    cmd.Parameters.AddWithValue("@password", password);
                    cmd.Parameters.AddWithValue("@mobileno", mobile_number);
                    cmd.Parameters.AddWithValue("@emailid", email_address);
                    cmd.Parameters.AddWithValue("@sqnumber", email_address);
                    cmd.Parameters.AddWithValue("@sqanswer", email_address);
                    int a = cmd.ExecuteNonQuery();
                    con.Close();
                    if (a > 0)
                    {
                        MsgBoxDisplay("User successfully create please login");
                    }
                    else
                    {
                        MsgBoxDisplay("Error while creating the User");
                    }
                }
                else
                {
                    con.Close();
                    MsgBoxDisplay("EMail ID/Username already Exists.");
                }
            }
            else
            {
                con.Close();
                MsgBoxDisplay("Please Provid Valid Details");
            }
        }
        catch (Exception ex)
        {
            con.Close();
            MsgBoxDisplay(ex.Message);
        }
    }

    private void clearRegistrationFields()
    {
        reg_fname.Value = "";
        reg_password.Value = "";
        reg_repassword.Value = "";
        mobile.Value = "";
        email_id.Value = "";
        securityQ.SelectedIndex = 0;
        securityA.Value = "";
        reg_captcha.Value = "";
    }

    public void clearLoginfields()
    {
        uname.Value = "";
        pwd.Value = "";
        txt_Code.Value = "";
    }

    private string getUserIP()
    {
        string ipList = HttpContext.Current.Request.ServerVariables["HTTP_X_CLUSTER_CLIENT_IP"];
        //HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (!string.IsNullOrEmpty(ipList))
        {
            ipList = ipList.Split(',')[0];
        }
        else
        {
            ipList = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList.GetValue(1).ToString();
            if (!string.IsNullOrEmpty(ipList))
            {
                ipList = ipList.Split(',')[0];
            }
            else
            {
                ipList = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
        }
        IPAddress ip = IPAddress.Parse(ipList);
        if (ip.AddressFamily == AddressFamily.InterNetwork)
        {
            return ipList;
        }
        else
        {
            return null;
        }
    }

    private bool validateInput(string stdtCode, string regExCode)
    {
        Regex r;
        Match m;
        try
        {
            if (string.IsNullOrEmpty(stdtCode))
            {
                return false;
            }
            else
            {
                r = new Regex(regExCode, RegexOptions.IgnoreCase);
                m = r.Match(stdtCode);
                if (!m.Success)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
            return false;
        }
    }

    protected string sha256encode(string value)
    {
        byte[] bytes = Encoding.UTF8.GetBytes(value.Trim().ToString());
        SHA256Managed hashstring = new SHA256Managed();
        byte[] hash = hashstring.ComputeHash(bytes);
        string hashString = string.Empty;
        foreach (byte x in hash)
        {
            hashString += String.Format("{0:x2}", x);
        }
        return hashString;
    }

    public void MsgBoxDisplay(string Message)
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "alertify.alert('Warning','" + Message + "')", true);
        //ScriptManager.RegisterStartupScript(this, GetType(), "msg", "<script>alert('" + Message + "');</script>", false);
    }
}