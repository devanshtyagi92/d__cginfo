var serviceLayer = "https://mapservice.gov.in/gismapservice/rest/services/BharatMapService/Admin_Boundary_District/MapServer?Token=HCg0nDjv_KOQQfbpd1UV0H2XBXtHqn9Uby214E73B3DtC7R9jPjYpkNMcsC9oWKM";
var mapOptions = {
    wkid: 102100,
    initialExtent: { xmin: 68.7, ymin: 8.4, xmax: 97.3, ymax: 37.6, spatialReference: { wkid: 4326 }},
    //initialExtent = new esri.geometry.Extent({ xmin: 77.23725451100006, ymin: 15.836406686000032, xmax: 81.31660838800008, ymax: 19.91690008100005, spatialReference: { wkid: 4326 } });
    fullExtent: { xmin: 66.500000, ymin: 4.700000, xmax: 98.000000, ymax: 40.000000 },
    lods: [], //{"level" : 0, "resolution" : 0.010986328125, "scale" : 4617149.97766929}
    showAttribution: false, //The attribution is displayed at the bottom of the map next
    showEsriLogo: false
};
var basemapsList = [
    {
	url:"https://mapservice.gov.in/mapserviceserv176/rest/services/Street/StreetMap/MapServer",
	token:"?token=M9nM5SBglQZ2i7nK9ZXY3XUM3-pc8uGIvVTP2q61-pb_sm9wwA6ZoUwtMO3pFaG6",
	options:{title:"NIC Street",thumbnailUrl:"img/basemapimages/NIC_Street.png"	}	
    },
    {
	    url:"http://services.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer",
	    token:"",
	    options:{title:"Satellite",thumbnailUrl:"img/basemapimages/Satellite.png"}
    }
];

var mapServiceUrl = basemapsList[0].url;//"https://mapservice.gov.in/mapserviceserv176/rest/services/Street/StreetMap/MapServer";
var tokenVar = basemapsList[0].token;//"?token=M9nM5SBglQZ2i7nK9ZXY3XUM3-pc8uGIvVTP2q61-pb_sm9wwA6ZoUwtMO3pFaG6";
var stateUrl = mapServiceUrl + "/1006" + tokenVar;
var districtUrl = mapServiceUrl + "/1007" + tokenVar;

var vil_service = "https://mapservice.gov.in/gismapservice/rest/services/BharatMapService/Admin_Boundary_Village/MapServer";
var vil_token = "?token=AYoPi0yUpPCJsWAW5QDg0EX9edXkdgxx8ENVhN_c6qu2fUja6AbX7JDkaFBCmaHIWzbBNl-Xf4Qghg5RXpLcNw..";

var dataObj = {
	facilityFields: [
		{
			facility_type: "Hospitals",
			subfacility_type: ["Community Health Centre", "Primary Health Centre", "Primary Health Sub Centre", "Maternity And Child Welfare Centre", "TB Clinic", "Hospital Allopathic", "Dispensary","Veterinary Hospital", "Mobile Health Clinic", "Family Welfare Centre", "Hospital"]
		},
		{
			facility_type: "Transporataion",
			subfacility_type: ["Public Bus Service", "Railway Station", "Taxi Stand", "Auto Stand", "Cycle Stand"]
		},
		{
			facility_type: "Water Bodies",
			subfacility_type: ["Hand Pump", "Tube Wells/Borehole", "Tank", "Pond", "Lake"]
		},
		{
			facility_type: "Others",
			subfacility_type: [
				"Community Toilet Complex", "Internet Cafes / Common Service Centre (CSC)", "Private Courier Facility", "Forest Office"]
		},
		{
			facility_type: "Post Office",
			subfacility_type: [
				"Branch Post Office", "Sub Post Office", "Head Post Office", "PO"]
		},
		{
			facility_type: "Schools",
			subfacility_type: [
				"Pre-Primary School", "Primary School", "Middle  School", "Secondary School", "Senior Secondary School", "Government School For Disabled","Categories(1-5,6-7,1-7,1-10,1-12,6-10,6-12,8-10,8-12,11-12)", "I.T.I", "Polytechnic", "Vocational Schools"] 
		},
		{
			facility_type: "Colleges",
			subfacility_type: [
				"Jr. College", "Degree College", "Professional Colleges(Engineering,Medical,BDS,MBA,IIT,IIM,IISC)University"]
		},
		{
			facility_type: "Agriculture",
			subfacility_type: [
				"KVK", "Fertilizer Department", "Seed Formation Department", "Cold Storage", "Agricultural Mandi", "Haat(Weekly)", "Nursery"]
		},
		{
			facility_type: "Animal Husbandry",
			subfacility_type: [
				"Veterinary Hospital", "Veterinary Clinic", "Insemination Center", "Gaushala"]
		},
		{
			facility_type: "Government Office",
			subfacility_type: [
				"Collector's Office/DM", "Revenue Sub Divisonal Officer's Office", "Tahasildar Office", "BDO Office", "Panchayat Bhavan", "Registrar's Office", "Sub Registrar's Office", "District Court", "Sub District Court", "Metropolitan Magistrate", "District Consumer Forum", "District Treasury", "Sub Treasury", "Fire Office", "DMHO", "ICDS Office", "Employment Exchange", "District Library", "DRDO", "DSWO (SC/ST)", "DM,Housing Corp.BC", "District Forest Office", "Divisional Forest Office", "District Horticulture Office", "District Sericulture Office", "Environment Engineer"]
		},
		{
			facility_type: "Engineering Offices",
			subfacility_type: [
				"PWD - CE/SE/EE / DYEE/AE", "PR - CE/SE/EE/DYEE/AE", "RWS - CE/SE/EE/DYEE/AE", "NH - EE/DYEE"]
		},
		{
			facility_type: "Irrigation",
			subfacility_type: [
				"Major - SE/EE/DYEE/AE", "Medium", "Minor"]
		},
		{
			facility_type: "Guest House",
			subfacility_type: [
				"PWD / Travellers Bungalow"]
		},
		{
			facility_type: "Tourist Places",
			subfacility_type: [""]
		},
		{
			facility_type: "Fair Price Shop",
			subfacility_type: [""]
		},
		{
			facility_type: "Heritage Spots / Monuments",
			subfacility_type: [""]
		}
	]
}
//####################################################### Data ############################################################//