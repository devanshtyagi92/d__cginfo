﻿var loadeddata_AddPoint;
var stcode, dtcode, chkGeometry;
var stname;
var dtname;
var ind_ext;
var log_Flag = false;
Get_All("", "");
function Get_All(user_mail, usertype) {
    require([
        "esri/map", "esri/layers/ArcGISDynamicMapServiceLayer", "esri/layers/FeatureLayer", "esri/symbols/TextSymbol", "esri/layers/LabelClass", "esri/symbols/Font", "esri/renderers/ClassBreaksRenderer",
        "esri/geometry/Extent", "esri/symbols/PictureMarkerSymbol", "esri/dijit/BasemapGallery", "esri/dijit/BasemapLayer", "esri/dijit/Basemap", "esri/dijit/BasemapToggle",
        "dojox/grid/EnhancedGrid",
        "dojo/data/ItemFileWriteStore",
        "esri/layers/GraphicsLayer",
        "esri/SpatialReference",
        "esri/geometry/webMercatorUtils",
        "esri/InfoTemplate",
        "esri/toolbars/draw",
        "esri/geometry/Point",
        "esri/symbols/SimpleMarkerSymbol",
        "esri/symbols/SimpleFillSymbol",
        "esri/symbols/SimpleLineSymbol",
        "esri/tasks/ProjectParameters",
        "esri/Color",
        "esri/graphic",
        "esri/graphicsUtils",
        "dojo/on",
        "dojo/_base/array",
        "dojo/dom", "dojo/_base/lang",
        "esri/tasks/QueryTask", "esri/tasks/query", "esri/tasks/IdentifyTask", "esri/tasks/IdentifyParameters",
        "dojox/grid/enhanced/plugins/Pagination",
        "dojo/domReady!"],
        function (Map, ArcGISDynamicMapServiceLayer, FeatureLayer, TextSymbol, LabelClass, Font, ClassBreaksRenderer, Extent, PictureMarkerSymbol, BasemapGallery, BasemapLayer, Basemap, BasemapToggle, EnhancedGrid, ItemFileWriteStore, GraphicsLayer, SpatialReference, webMercatorUtils, InfoTemplate, Draw,
            Point, SimpleMarkerSymbol, SimpleFillSymbol, SimpleLineSymbol, ProjectParameters, Color, Graphic, graphicsUtils, on, array, dom, lang, QueryTask, Query, IdentifyTask, IdentifyParameters) {
            //####################################################### OnCreateFunctions ############################################################//
            if (user_mail == "") {
                _createMapFromOptions();
                _GetAllDataRequest("");
            }
            else {
                if (usertype == "S_User") {
                    user_mail = user_mail;
                    _GetAllDataRequest(user_mail);
                }
                else if (usertype == "") {
                    user_mail = user_mail;
                    _GetAllDataRequest(user_mail);
                    //_GetStateWiseDataRequest(user_mail);
                }
            }
            //_GetAllDataRequest();
            var selectgl = new GraphicsLayer();

            esri.config.defaults.io.corsEnabledServers.push("http://localhost:52348/")
            var selsymb = new SimpleFillSymbol(SimpleFillSymbol.STYLE_NULL, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([0, 255, 0, 1]), 4), null);
            //####################################################### OnCreateFunctions ############################################################//

            //####################################################### Map Creation ############################################################//
            function _createMapFromOptions() {
                var initialExtentConfig, fullExtentConfig, lodsConfig, showAttributionConfig, wkidConfig, wkid;
                var mapOptionsObj = new Object();
                try {
                    dom.byId("bodyloadimg").style.display = "block";
                    mapOptionsObj.logo = mapOptions.showEsriLogo;
                    mapOptionsObj.showAttribution = mapOptions.showAttribution;
                    mapOptionsObj.basemap = "topo";
                    mapOptionsObj.minZoom = 5;
                    mapOptionsObj.sliderPosition = "top-left";
                    mapOptionsObj.sliderStyle = "large";
                }
                catch (err) {
                    console.log(err);
                }
                finally {
                    var basemaps = [];
                    map = new Map("map", mapOptionsObj);
                    var nicstreetlayer = new BasemapLayer({
                        url: mapServiceUrl + tokenVar
                    });
                    var nicstreetbasemap = new Basemap({
                        layers: [nicstreetlayer],
                        title: "NIC Street",
                        thumbnailUrl: "img/basemapimages/NIC_Street.png"
                    });
                    basemaps.push(nicstreetbasemap);
                    var satellitelayer = new BasemapLayer({
                        url: "http://services.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer"
                    });
                    var satellitebasemap = new Basemap({
                        layers: [satellitelayer],
                        title: "Satellite",
                        thumbnailUrl: "img/basemapimages/Satellite.png"
                    });
                    basemaps.push(satellitebasemap);
                    var basemapGallery = new BasemapGallery({
                        showArcGISBasemaps: false,
                        basemaps: basemaps,
                        map: map
                    }, basemapGallery);
                    basemapGallery.on("error", function (msg) {
                        console.log("basemap gallery error:  ", msg);
                        basemapGallery.select("basemap_1");
                    });
                    basemapGallery.startup();
                    var admin = new ArcGISDynamicMapServiceLayer(vil_service + vil_token);
                    map.addLayers([admin]);
                    admin.setVisibleLayers([0, 1, 3]);
                    ind_ext = new Extent(66.62, 5.23, 98.87, 38.59, new SpatialReference({ wkid: 4326 }));
                    map.setExtent(ind_ext.expand(1.2));
                    basemapFlag = "1";
                    $("#BasemapToggle").on("click", function (event) {
                        if (basemapFlag == "0") {
                            basemapFlag = "1";
                            basemapGallery.select("basemap_0");
                            document.getElementById("basemapImage").src = "img/basemapimages/Satellite.png";
                            document.getElementById("basemapImage").title = "Satellite";
                        }
                        else if (basemapFlag == "1") {
                            basemapFlag = "0";
                            basemapGallery.select("basemap_1");
                            document.getElementById("basemapImage").src = "img/basemapimages/NIC_Street.png";
                            document.getElementById("basemapImage").title = "NIC Street";
                        }
                    });

                    $("#signup_cncl").click(function () {
                        signup_close();
                    });
                    $("#cls_signup").click(function () {
                        signup_close();
                    });
                    function signup_close() {
                        document.getElementById('uname1').value = '';
                        document.getElementById('chkUsr').style.display = 'none';
                        document.getElementById('pwd1').value = '';
                        $("#showpwmsg").hide();
                        document.getElementById('pwd2').value = '';
                        document.getElementById('mobNo').value = '';
                        document.getElementById('emailID').value = '';
                        $("#sQuestion option").filter(function () { return $(this).text() == "Choose your Question?"; }).prop('selected', true);
                        document.getElementById('sQAns').value = '';
                        document.getElementById('txt_Code1').value = '';
                    }
                    $("#cls_login").click(function () {
                        login_close();
                    });
                    $("#login_cncl").click(function () {
                        login_close();
                    });
                    $("#clear").click(function () {
                        map.graphics.clear();
                    });
                    function login_close() {
                        document.getElementById('uname').value = '';
                        document.getElementById('pwd').value = '';
                        document.getElementById('txt_Code').value = '';
                    }
                    var point_graphlayer = new GraphicsLayer();
                    map.addLayer(point_graphlayer);
                    map.on("load", function () {
                        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(showPosition);
                        } else console.log("Location permission denied.");//alert("");
                        basemapGallery.select("basemap_0");
                        map.disableDoubleClickZoom();
                        map.setExtent(mapOptions.initialExtent);

                    });
                    function showPosition(position) {
                        var geometry = webMercatorUtils.geographicToWebMercator(new Point(position.coords.longitude, position.coords.latitude));
                        map.centerAndZoom(geometry, 12);
                        var pt = new Point(Number(position.coords.longitude), Number(position.coords.latitude), new SpatialReference({ wkid: 4326 }));
                        var markersymbol = new PictureMarkerSymbol({
                            "url": "img/livepoint.gif",
                            "height": "25",
                            "width": "18",
                            "yoffset": 15,
                            "xoffset": 0
                        });

                        point_graphlayer.add(new Graphic(pt, markersymbol, null, null));

                    }
                    s_point_flag = true;
                    map.on("click", function (evt) {

                        ////AddFeaturesFromMIS();

                        //var point = new Point(evt.mapPoint);
                        //var Lat = point.getLatitude().toFixed(4);
                        //var Long = point.getLongitude().toFixed(4);
                        //var m_point = false;
                        //var points = PointsGraphicLayer.graphics;
                        //for (var p = 0; p < points.length; p++) {
                        //    if (Number(points[p].attributes.Latitude).toFixed(4) != Lat && Number(points[p].attributes.Longitude).toFixed(4) != Long)
                        //        m_point = true;
                        //}
                        if (s_point_flag)
                            addToMap(evt);
                        else
                            assetmouseoutfun();
                    });
                    map.on("dbl-click", function (evt) {
                        dom.byId("bodyloadimg").style.display = "block";
                        // identifyTask = new IdentifyTask(mapServiceUrl + tokenVar);
                        identifyTask = new IdentifyTask(vil_service + vil_token);

                        identifyParams = new IdentifyParameters();
                        identifyParams.tolerance = 3;
                        identifyParams.returnGeometry = true;
                        // identifyParams.layerIds = [1007];
                        identifyParams.layerIds = [1];
                        identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_ALL;
                        identifyParams.width = map.width;
                        identifyParams.height = map.height;

                        map.graphics.clear();
                        identifyParams.geometry = event.mapPoint;
                        identifyParams.mapExtent = map.extent;
                        identifyTask.execute(identifyParams, function (idResults) {
                            if (idResults.length > 0) {
                                stcode = idResults[0].feature.attributes.stcode11;
                                dtcode = idResults[0].feature.attributes.dtcode11;
                                stname = idResults[0].feature.attributes.stname;
                                dtname = idResults[0].feature.attributes.dtname;
                                fillFacilitiesData();

                                map.setExtent(ind_ext = idResults[0].feature.geometry.getExtent());
                                if (selectgl)
                                    selectgl.clear();
                                selectgl.add(new Graphic(chkGeometry = idResults[0].feature.geometry, selsymb));
                                map.addLayer(selectgl);

                                var first = document.getElementById("statedd").options[document.getElementById("statedd").selectedIndex].text;
                                var len = document.getElementById("statedd").options.length;
                                if (first != idResults[0].feature.attributes.stname) {
                                    for (var i = 0; i < len; i++) {
                                        if (document.getElementById("statedd").options[i].text == idResults[0].feature.attributes.stname) {
                                            document.getElementById("statedd").selectedIndex = i;

                                            var query = new Query();
                                            query.where = "stcode11 ='" + dom.byId("statedd").options[dom.byId("statedd").selectedIndex].value + "'";
                                            query.outFields = ["stcode11", "dtcode11", "dtname"];
                                            //var queryTask = new QueryTask(districtUrl);
                                            var queryTask = new QueryTask(vil_service + "/1" + vil_token);
                                            queryTask.execute(query, dojo.partial(stdtbkfun, "dtname", "dtcode11", "Select District", "districtdd"));

                                            setTimeout(function () { assignQueryDetails(dtcode, idResults[0].feature.attributes.dtname) }, 2000);
                                            break;
                                        }
                                    }
                                } else
                                    assignQueryDetails(dtcode, idResults[0].feature.attributes.dtname);
                            }
                            dom.byId("bodyloadimg").style.display = "none";
                        });
                    });
                    dom.byId("bodyloadimg").style.display = "none";
                }
            }

            function assignQueryDetails(dtcode, dtname) {

                //dom.byId("districtdd").selectedIndex = 0;
                dom.byId("districtdd").style.visibility = "visible";
                dom.byId("districtdd").style.visibility = "visible";
                var first = document.getElementById("districtdd").options[document.getElementById("districtdd").selectedIndex].text;
                var len = document.getElementById("districtdd").options.length;
                if (first != dtname) {
                    for (var i = 0; i < len; i++) {
                        if (document.getElementById("districtdd").options[i].text == dtname) {
                            document.getElementById("districtdd").selectedIndex = i;

                            //var query = new Query();
                            //query.where = "dtcode11 = '" + dtcode + "'";
                            //query.returnGeometry = true;
                            //var queryTask = new QueryTask(districtUrl);
                            //queryTask.execute(query, dojo.partial(stdtbkfun_ext));

                            break;
                        }
                    }
                }
            }

            //####################################################### Map Creation ############################################################//
            //----------------------------------------------------Data From Map Service--------------------------------------//
            var query = new Query();
            query.where = "1=1";
            query.outFields = ["STCODE11", "STNAME"];
            var queryTask = new QueryTask(vil_service + "/0" + vil_token);
            query.orderByFields = ["STNAME"];
            query.returnGeometry = false;
            queryTask.execute(query, FillStates);
            function FillStates(featureSet) {
                var x = document.getElementById("statedd");
                for (var i = 0; len = featureSet.features.length, i < len; i++) {
                    //Add options to Level combobox
                    var option = document.createElement("option");
                    option.text = featureSet.features[i].attributes.STNAME.toString();//["stname"].toString();
                    option.value = featureSet.features[i].attributes.STCODE11.toString();//["stcode11"].toString();
                    x.add(option);
                    x.selectedIndex = 0;
                }
            }

            on(dom.byId("statedd"), "change", function () {
                stcode = dom.byId("statedd").options[dom.byId("statedd").selectedIndex].value;
                if (stcode == "select") {
                    dom.byId("districtdd").style.visibility = "hidden";
                    dom.byId("districtdd").style.visibility = "hidden";
                    //map.setExtent(ind_ext, true);
                } else {
                    var query = new Query();
                    query.where = "stcode11 ='" + stcode + "'";
                    query.outFields = ["stcode11", "dtcode11", "dtname"];
                    var queryTask = new QueryTask(vil_service + "/1" + vil_token);
                    queryTask.execute(query, dojo.partial(stdtbkfun, "dtname", "dtcode11", "Select District", "districtdd"));
                    dom.byId("districtdd").selectedIndex = 0;
                    dom.byId("districtdd").style.visibility = "visible";
                    dom.byId("districtdd").style.visibility = "visible";
                }
                var query = new Query();
                query.where = "STCODE11 = '" + stcode + "'";
                query.returnGeometry = true;
                var queryTask = new QueryTask(vil_service + "/0" + vil_token);
                queryTask.execute(query, dojo.partial(stdtbkfun_ext));
            });

            on(dom.byId("districtdd"), "change", function () {
                stcode = dom.byId("statedd").options[dom.byId("statedd").selectedIndex].value;
                dtcode = dom.byId("districtdd").options[dom.byId("districtdd").selectedIndex].value;
                if (dtcode == "select") {
                    var query = new Query();
                    query.where = "stcode11 = '" + stcode + "'";
                    query.returnGeometry = true;
                    //var queryTask = new QueryTask(stateUrl);
                    var queryTask = new QueryTask(vil_service + "/0" + vil_token);
                    queryTask.execute(query, dojo.partial(stdtbkfun_ext));
                    return;
                } else {
                    var query = new Query();
                    query.where = "dtcode11 = '" + dtcode + "'";
                    query.returnGeometry = true;
                    //var queryTask = new QueryTask(districtUrl);
                    var queryTask = new QueryTask(vil_service + "/1" + vil_token);
                    queryTask.execute(query, dojo.partial(stdtbkfun_ext));
                    fillFacilitiesData();
                }
            });
            fillFacilitiesData();
            function fillFacilitiesData() {
                var facilityData = dom.byId("selectorInput");
                while (facilityData.firstChild) {
                    facilityData.removeChild(facilityData.firstChild);
                }
                var option = document.createElement("option");
                option.value = "Select";
                option.text = "Select Facility";
                facilityData.add(option);
                var tmpArray = new Array();
                for (var i = 0; i < dataObj.facilityFields.length; i++) {
                    tmpArray[i] = new Array();
                    tmpArray[i][0] = dataObj.facilityFields[i].facility_type;
                    //tmpArray[i][1] = i;
                }
                tmpArray.sort();
                for (var i = 0; i < tmpArray.length; i++) {
                    var op = new Option(tmpArray[i][0], tmpArray[i][0]);
                    facilityData.options[i + 1] = op;
                }
            }

            //########Geo Coder

            on(dom.byId("txt_srch"), "keyup", function (e) {
                var keyCode = e.which ? e.which : e.keyCode;
                if (keyCode == 40) {
                    document.getElementById("srch_list").focus();
                }
                var edValue = document.getElementById("txt_srch");
                var str;
                var s = edValue.value;
                if (s && s.length >= 1) {
                    var firstChar = s.charAt(0);
                    var remainingStr = s.slice(1);
                    str = firstChar.toUpperCase() + remainingStr;

                    var query = new Query();
                    if ($("#statedd").val() == "select") {
                        query.where = "(VILNAME11 like '" + s.toUpperCase() + "%')  OR (VILNAME11 like '" + s.toLowerCase() + "%'" + ") OR   (VILNAME11 like '" + str + "%') ";
                    }
                    else if ($("#statedd").val() != "select" && $("#districtdd").val() == "select") {
                        query.where = "(STCODE11 = '" + stcode + "' AND  VILNAME11 like '" + s.toUpperCase() + "%')  OR (STCODE11 = '" + stcode + "' AND  VILNAME11 like '" + s.toLowerCase() + "%'" + ") OR   (STCODE11 = '" + stcode + "' AND  VILNAME11 like '" + str + "%') ";
                    }
                    else if ($("#statedd").val() != "select" && $("#districtdd").val() != "select") {
                        query.where = "(DTCODE11 = '" + dtcode + "' AND  VILNAME11 like '" + s.toUpperCase() + "%')  OR (DTCODE11 = '" + dtcode + "' AND  VILNAME11 like '" + s.toLowerCase() + "%'" + ") OR   (DTCODE11 = '" + dtcode + "' AND  VILNAME11 like '" + str + "%') ";
                    }
                    query.outFields = ["*"];
                    var queryTask = new QueryTask(vil_service + "/3" + vil_token);
                    queryTask.execute(query, dojo.partial(twnlist, "VILNAME11", "VILCODE11"));
                }
                else {
                    document.getElementById("srch_list").style.display = "none";
                }
            });

            function compareStrings(a, b) {
                // Assuming you want case-insensitive comparison
                //  a = a.toLowerCase();
                //  b = b.toLowerCase();

                return (a < b) ? -1 : (a > b) ? 1 : 0;
            }

            function twnlist(vname, vcode, fs) {

                var fet = fs.features;
                var tmpAry = new Array();
                var uniqueNames = [];
                var grades = [];
                var temp = [];


                for (var k = 0; k < fet.length; k++) {
                    uniqueNames.push(fet[k].attributes[vname]);
                    tmpAry.push({ "name": fet[k].attributes[vname], "code": fet[k].attributes[vcode] });

                }
                //remove duplicates from array
                tmpAry = tmpAry.filter((x, i) => {
                    if (temp.indexOf(x.code) < 0) {
                        temp.push(x.code);
                        return true;
                    }
                    return false;
                })
                //sort array using name of the village
                tmpAry.sort(function (a, b) {
                    return compareStrings(a.name, b.name);
                });

                var listofdata = document.getElementById("srch_list");
                while (listofdata.firstChild) {
                    listofdata.removeChild(listofdata.firstChild);
                }
                for (var k = 0; k < tmpAry.length; k++) {
                    var option = document.createElement('option');
                    option.value = tmpAry[k].code;
                    option.text = tmpAry[k].name;
                    listofdata.add(option);
                }
                document.getElementById("srch_list").style.display = "block";
            }

            on(dom.byId("srch_list"), "click", function () {
                var villageselect = document.getElementById("srch_list");
                var selectedText = villageselect.options[villageselect.selectedIndex].text;
                var sn = villageselect.options[villageselect.selectedIndex].value;

                document.getElementById("srch_list").style.display = "none";
                document.getElementById("txt_srch").value = selectedText;// sn;
                var query = new Query();
                var queryWhere = "";
                query.where = "VILCODE11='" + sn + "'";

                query.outFields = ["VILNAME11"];
                query.returnGeometry = true;
                query.outSpatialReference = { wkid: 4326 };
                //query.outSpatialReference = map.spatialReference;
                var queryTask = new QueryTask(vil_service + "/3" + vil_token);
                queryTask.execute(query, dojo.partial(zooming_vil, "VILNAME11"));
                function zooming_vil(vname, fs) {
                    try {
                        var symbol = new esri.symbol.SimpleMarkerSymbol();
                        symbol.style = esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE;
                        symbol.setSize(10); symbol.setColor(new dojo.Color([0, 0, 0, 255]));
                        var graphic = new Graphic(fs.features[0].geometry, symbol);
                        map.graphics.clear();
                        map.graphics.add(graphic);
                        var mp = fs.features[0].geometry;
                        var newExtent = new esri.geometry.Extent();
                        newExtent.xmin = mp.x + 0.1;
                        newExtent.ymin = mp.y - 0.1;
                        newExtent.xmax = mp.x - 0.1;
                        newExtent.ymax = mp.y + 0.1;
                        map.setExtent(mp.getExtent());
                    }
                    catch (error) {
                        console.log(error);
                    }
                }
            });

            //###### End Geo Coder

            $('#selectorInput').on('change', function (evt) {
                //alert('here');
                var facilityData = dom.byId("selectorInput2");
                while (facilityData.firstChild) {
                    facilityData.removeChild(facilityData.firstChild);
                }
                var option = document.createElement("option");
                option.value = "Select";
                option.text = "Select Sub Facility";
                facilityData.add(option);
                var tempAry = [];
                var facility_typename = $('#selectorInput')[0].value;
                document.getElementById('selectorInput_txt').value = facility_typename;
                for (var j = 0; j < dataObj.facilityFields.length; j++) {
                    $("#selectorInput2").prop("disabled", false);
                    if (facility_typename == "Tourist Places" || facility_typename == "Fair Price Shop" || facility_typename == "Heritage Spots / Monuments") {
                        $("#selectorInput2").prop("disabled", true);
                    }
                    else if (facility_typename == dataObj.facilityFields[j].facility_type) {
                        for (var k = 0; k < dataObj.facilityFields[j].subfacility_type.length; k++) {
                            tempAry[k] = new Array();
                            tempAry[k][0] = dataObj.facilityFields[j].subfacility_type[k];
                        }
                    }
                }
                //tempAry.sort();
                for (i in tempAry) {
                    var op = new Option(tempAry[i]);
                    facilityData.add(op);
                    //facilityData.options[i + 1] = op;
                }
            });
            $('#selectorInput2').on('change', function (evt) {
                document.getElementById('selectorInput2_txt').value = $('#selectorInput2')[0].value;
            });
            function stdtbkfun_ext(fs) {
                if (selectgl)
                    selectgl.clear();
                if (stcode == "select") {
                    ind_ext = new Extent(68.09347, 6.75436, 97.41149, 37.07761, new SpatialReference({ wkid: 4326 })); //66.7248, 5.3169, 98.8766, 38.9386,
                    map.setExtent(ind_ext, true);
                }
                else {
                    ind_ext = fs.features[0].geometry.getExtent();
                    selectgl.add(new Graphic(fs.features[0].geometry, selsymb));
                    map.setExtent(ind_ext, true);
                    map.addLayer(selectgl);
                }
            }
            $("#GridBtn").on("click", function () {
                dom.byId("DataContent").style.visibility = "visible";
                dragElement(dom.byId("DataContent"));
            });

            $(".toggle-password").click(function () {

                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });

            document.getElementById("txt_srch").addEventListener("search", function (event) {
                map.graphics.clear();
                document.getElementById("srch_list").style.display = "none";
                map.setExtent(ind_ext.expand(1.2));
            });
            function stdtbkfun(name, code, stdtbk, stdtbkdd, fs) {
                var stdtbkdd = dom.byId(stdtbkdd);
                while (stdtbkdd.firstChild) {
                    stdtbkdd.removeChild(stdtbkdd.firstChild);
                }
                var option = document.createElement('option');
                option.value = "select";
                option.text = stdtbk;
                stdtbkdd.add(option);
                var fet = fs.features;
                var tmpAry = new Array();
                for (var k = 0; k < fet.length; k++) {
                    tmpAry[k] = new Array();
                    tmpAry[k][0] = fet[k].attributes[name];
                    tmpAry[k][1] = fet[k].attributes[code];
                }
                tmpAry.sort();
                //selectgl.clear();
                for (var o = 0; o < tmpAry.length; o++) {
                    var op = new Option(tmpAry[o][0], tmpAry[o][1]);
                    stdtbkdd.options[o + 1] = op;
                }
            }
            //####################################################### Data Retrieval ############################################################//
            //################################################### InfoTemplate Generation ########################################################//
            //var InfoTitle = "Points Data";
            //var InfoContent = _generateInfoTemplateContent(FeatureCollectionFields);
            //var infoTemplate = new InfoTemplate(InfoTitle, InfoContent);
            function _GetAllDataRequest(usermail) {
                //dom.byId("loading").style.display = "block";
                //var MIScontent;
                $.ajax({
                    type: "GET",
                    ////data: { "UserMail": usermail },
                    url: "https://bharatnetprogress.nic.in/POI_API/api/cginfoapi/GetAllUserData?UserName=" + usermail,
                    //url: "http://10.1.31.30/cginfoapi/api/cginfoapi/GetAllUserData?UserName=" + usermail,
                    dataType: "JSON",
                    contentType: 'application/x-www-form-urlencoded',
                    crossDomain: true,
                    success: function (dat) {
                        MIS_rec = dat;
                        if (MIS_rec[0].FacilitiesData != null) {
                            MIS_rec_images = [];
                            MIS_rec = MIS_rec[0].FacilitiesData;
                            for (var m = 0; m < MIS_rec.length; m++) {
                                MIS_rec_images.push({
                                    Img_Uid: MIS_rec[m].uniqueid,
                                    Img_Path: MIS_rec[m].imagepath,
                                    Sub_info: MIS_rec[m].subinfotype
                                });
                            }
                            AddFeaturesFromMIS();
                            _GenerateGrid(MIS_rec);

                            dom.byId("bodyloadimg").style.display = "none";
                        } else {
                            //alert("MIS data not available");
                            //------for first time insertion into database(if zero records found)-----
                            //Drawtoolbar = new Draw(map);
                            //Drawtoolbar.on("draw-end", addToMap);
                            dom.byId("bodyloadimg").style.display = "none";
                            //----------
                        }
                    },
                    error: function (err) {
                        //alert("error");
                        console.log(err);
                    }
                });
            }
            //function AddFeaturesFromMIS() {
            //    var features = [];
            //    if (PointsGraphicLayer == null) {
            //        PointsGraphicLayer = new GraphicsLayer();
            //        PointsGraphicLayer.setInfoTemplate(infoTemplate);
            //        map.addLayers([PointsGraphicLayer]);

            //        var graphlayerhandle = on.pausable(PointsGraphicLayer, "mouse-out", assetmouseoutfun);
            //        //PointsGraphicLayer.on("mouse-over", dojo.partial(assetmouseoverfun, graphlayerhandle));
            //        PointsGraphicLayer.on("click", dojo.partial(assetmouseclickfun, graphlayerhandle));


            //        //Drawtoolbar = new Draw(map);
            //        //Drawtoolbar.on("draw-end", addToMap);
            //    } else {
            //        PointsGraphicLayer.clear();
            //    }
            //    var symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 10, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
            //        new Color([192, 37, 146]), 3), new Color([255, 255, 0]));
            //    array.forEach(MIS_rec, function (feature) {
            //        var attr = {};
            //        attr["Latitude"] = feature.latitude == undefined ? 'NA' : feature.latitude;
            //        attr["Longitude"] = feature.longitude == undefined ? 'NA' : feature.longitude;
            //        attr["InfoName"] = feature.infoname == undefined ? 'NA' : feature.infoname;
            //        attr["UniqueID"] = feature.UniqueID == undefined ? 'NA' : feature.UniqueID;
            //        attr["InfoType"] = feature.infotype == undefined ? 'NA' : feature.infotype;
            //        attr["SubInfoType"] = feature.subinfotype == undefined ? 'NA' : feature.subinfotype;
            //        attr["InfoAddress"] = feature.infoaddress == undefined ? 'NA' : feature.infoaddress;
            //        attr["Description"] = feature.description == undefined ? 'NA' : feature.description;
            //        attr["ContactNo"] = feature.contactno == undefined ? 'NA' : feature.contactno;
            //        attr["ContactMailID"] = feature.contactmailid == undefined ? 'NA' : feature.contactmailid;
            //        if (feature.imagepath != null && feature.imagepath != 'No Image Found') {
            //            attr["Picture"] = feature.imagepath;
            //        }
            //        else {
            //            attr["Picture"] = "img/nopreview.jpg";
            //        }
            //        var geometry = new Point(feature.longitude, feature.latitude);
            //        var graphic = new Graphic(geometry, symbol, attr);
            //        features.push(graphic);
            //        PointsGraphicLayer.add(graphic);
            //    });
            //    var PointsExtent = graphicsUtils.graphicsExtent(features);
            //    //map.setExtent(PointsExtent.expand(3));
            //}
            //function assetmouseclickfun(asset_graphlayerhandle, event) {
            //    asset_graphlayerhandle.pause();
            //    s_point_flag = false;
            //    $('.titleButton.close').click(function () {
            //        s_point_flag = true;
            //    });
            //}
            //function assetmouseoutfun() {
            //    //map.infoWindow.hide();
            //    s_point_flag = true;
            //}
            //function assetmouseoverfun(asset_graphlayerhandle, event) {
            //    asset_graphlayerhandle.resume();
            //    map.infoWindow.clearFeatures();
            //    var graphic12 = event.graphic;
            //    map.infoWindow.setContent(graphic12.getContent());
            //    map.infoWindow.setTitle(graphic12.getTitle());
            //    map.infoWindow.show(event.screenPoint, map.getInfoWindowAnchor(event.screenPoint));
            //}
            //function _generateInfoTemplateContent(fields) {
            //    var content = "<table>";
            //    var fieldsLength = fields.length;
            //    for (var x = 0; x < fieldsLength; x++) {
            //        var field = fields[x];
            //        if (field.alias == "Picture") {
            //            content += "<tr><td colspan=3><a  onclick=\"window.open(this.href, 'popUpWindow', 'width=562, height=487, scrollbars=1').focus(); return false;\" target='_blank' href=\"javascript:document.write('<img src=data:image/jpg;base64,${" + field.name + "} width=540 height=465 style = \\'margin: 0px auto;display: block;\\' />')\"><img src=data:image/jpg;base64,${" + field.name + "} alt='No Image Found' width=230 height=110/></a></td></tr>";
            //            //content += "<tr><td colspan='3'><img src='data:image/jpg;base64,${" + field.name + "}' style='max-width:80%;max-height:80%;'/>  </td> </tr>";
            //            //"<tr><td colspan=3><a  onclick=\"window.open(this.href, 'popUpWindow', 'width=562, height=487, scrollbars=1').focus(); return false;\" target='_blank' href=\"javascript:document.write('<img src=data:image/jpg;base64,${" + field.name + "} width=540 height=465 style = \\'margin: 0px auto;display: block;\\' />')\"><img src=data:image/jpg;base64,${" + field.name + "} alt='img' width=230 height=110/></a></td></tr>";
            //        } else {
            //            content += "<tr><td>" + field.alias + "</td> <td>:</td> <td>  ${" + field.name + "} </td> </tr>";
            //        }
            //    }
            //    content += "</table>"
            //    return content;
            //}
            //################################################### InfoTemplate Generation ########################################################//
            //############################################### Generate Feature Collection ########################################################//

            //############################################### Generate Feature Collection ########################################################//
            //####################################################### Grid Generation ############################################################//
            function _GenerateGrid(MIS_rec) {
                for (var s = 0; s < MIS_rec.length; s++) {
                    s
                    MIS_rec[s].SlNo = s + 1;
                    //MIS_rec[s].image = "<img src='data:image/jpg;base64," + MIS_rec[s].imagepath + "' style='width:50px; height:50px;'>";
                }
                var GridData = {
                    identifier: "uniqueid",
                    label: "uniqueid",
                    items: MIS_rec
                };
                var GridStore = new ItemFileWriteStore({
                    data: GridData
                });
                var gridWidth = $(window).width() * 0.38;
                var grid = new EnhancedGrid({
                    width: gridWidth,
                    store: GridStore,
                    structure: MISDataLayoutArray,
                    columnReordering: true,
                    loadingMessage: "Data loading",
                    rowsPerPage: 10,
                    updateDelay: 0,
                    plugins: {
                        pagination: {
                            pageSizes: ["10", "20", "30", "All"],
                            description: true,
                            sizeSwitch: true,
                            pageStepper: true,
                            gotoButton: true,
                            maxPageStep: 4,
                            position: "bottom"
                        }
                    }
                });


                var GridDiv = dom.byId("Grid");
                GridDiv.innerHTML = "";
                grid.placeAt(GridDiv);
                grid.startup();
                grid.on("rowclick", _onRowClickHandler);
            }
            //####################################################### Grid Generation ############################################################//
            //######################################################### Grid OnClick #############################################################//
            var InfoArray = [];
            function _onRowClickHandler(evt) {
                $("#lblMessage").text("");
                $("#FileUpload").val("");
                map.graphics.clear();
                var ContentDiv = document.getElementById("InfoContent");
                ContentDiv.innerHTML = "";
                var UID = evt.grid.getItem(evt.rowIndex)['uniqueid'] == undefined ? 'NA' : evt.grid.getItem(evt.rowIndex)['uniqueid'][0];
                var Lat = evt.grid.getItem(evt.rowIndex)['latitude'] == undefined ? 'NA' : evt.grid.getItem(evt.rowIndex)['latitude'][0];
                var Long = evt.grid.getItem(evt.rowIndex)['longitude'] == undefined ? 'NA' : evt.grid.getItem(evt.rowIndex)['longitude'][0];
                var InfoName = evt.grid.getItem(evt.rowIndex)['infoname'] == undefined ? 'NA' : evt.grid.getItem(evt.rowIndex)['infoname'][0];
                var InfoType = evt.grid.getItem(evt.rowIndex)['infotype'] == undefined ? 'NA' : evt.grid.getItem(evt.rowIndex)['infotype'][0];
                var SubInfoType = evt.grid.getItem(evt.rowIndex)['subinfotype'] == undefined ? 'NA' : evt.grid.getItem(evt.rowIndex)['subinfotype'][0];
                var InfoAddress = evt.grid.getItem(evt.rowIndex)['infoaddress'] == undefined ? 'NA' : evt.grid.getItem(evt.rowIndex)['infoaddress'][0];
                var Description = evt.grid.getItem(evt.rowIndex)['description'] == undefined ? 'NA' : evt.grid.getItem(evt.rowIndex)['description'][0];
                var ContactNo = evt.grid.getItem(evt.rowIndex)['contactno'] == undefined ? 'NA' : evt.grid.getItem(evt.rowIndex)['contactno'][0];
                var ContactMailID = evt.grid.getItem(evt.rowIndex)['contactmailid'] == undefined ? 'NA' : evt.grid.getItem(evt.rowIndex)['contactmailid'][0];
                map.centerAndZoom([Long, Lat], 10);
                var symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 10, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                    new Color([0, 0, 255]), 3), new Color([0, 255, 255]));
                var geometry = new Point(Long, Lat);
                var graphic = new Graphic(geometry, symbol);
                map.graphics.add(graphic);
                InfoArray = [
                    //{
                    //"alias": "Sl.No",
                    //"value": UID
                    //},
                    {
                        "alias": "Latitude",
                        "value": Lat
                    }, {
                        "alias": "Longitude",
                        "value": Long
                    }, {
                        "alias": "Facility Name",
                        "value": InfoName
                    }, {
                        "alias": "Facility Type",
                        "value": InfoType
                    }, {
                        "alias": "Sub Facility Type",
                        "value": SubInfoType
                    }, {
                        "alias": "InfoAddress",
                        "value": InfoAddress
                    }, {
                        "alias": "Description",
                        "value": Description
                    }, {
                        "alias": "Contact Number",
                        "value": ContactNo
                    }, {
                        "alias": "Contact Mail ID",
                        "value": ContactMailID
                    }];
                var Content = _generateContentForInfoWindow(InfoArray);
                ContentDiv.innerHTML = Content;
                $("#InfoDialog").show();
                $("#ImagePreview").show();
                _createImageFromData(UID);
            }

            function _generateContentForInfoWindow(fields) {
                var content = "<table style='width:90%; border:1px solid #000;'>";
                var fieldsLength = fields.length;
                for (var x = 0; x < fieldsLength; x++) {
                    var field = fields[x];
                    content += "<tr><td style='width:34%; border:1px solid #000;'>" + field.alias + "</td> <td style='width:1%; border:1px solid #000;'>:</td> <td style='width:55%; border:1px solid #000;'>" + field.value + "</td></tr>";
                }
                content += "</table>"
                return content;
            }
            //######################################################### Grid OnClick #############################################################//
            function _createImageFromData(uid) {
                var ContentDiv = document.getElementById("ImageContainer");
                ContentDiv.innerHTML = "";
                for (var im = 0; im < MIS_rec_images.length; im++) {
                    if (MIS_rec_images[im].Img_Uid == uid) {
                        //var unquoted = result.replace(/\"/g, "");
                        var unquoted = MIS_rec_images[im].Img_Path;
                        subinfo_type = MIS_rec_images[im].Sub_info;
                        u_id = MIS_rec_images[im].Img_Uid;
                    }
                }

                if (unquoted != "") {
                    $("#ImageContainer").append("<img id='myImage' src='data:image/jpg;base64," + unquoted + "' style='max-width:80%;max-height:80%;'/>");
                } else {
                    $("#ImageContainer").append("<img id='myImage' src='img/nopreview.jpg' style='max-width:80%;max-height:80%;'/>");
                    $("#lblMessage").text("The Selected record does not have any Images");
                }
            }
            //###################################################### Get Image from DB ###########################################################//
            //####################################################### Send Image to DB ###########################################################//
            var loadeddata = "";
            //document.getElementById("FileUpload").addEventListener("change", readFile);

            function readFile() {
                if (this.files && this.files[0]) {
                    if (this.files[0].type == "image/jpeg" || this.files[0].size > 300000) {
                        var FR = new FileReader();
                        FR.addEventListener("load", function (e) {
                            var ContentDiv = document.getElementById("ImageContainer");
                            ContentDiv.innerHTML = "";
                            var data = e.target.result;
                            loadeddata = data.split(",").pop();
                            $("#ImageContainer").append("<img id='myImage' src='data:image/jpg;base64," + loadeddata + "' style='max-width:80%;max-height:80%;'/>");
                            $("#lblMessage").text("Click on Upload to save the previewed Image to this Location.");
                        });
                        FR.readAsDataURL(this.files[0]);
                    } else {
                        $("#lblMessage").text("The File added is invalid. Please choose a .jpg image of file size less than 300kb");
                    }
                }
            }


            $("#btnUpload").on("click", function () {
                _SendImagetoSever(loadeddata);
            });

            $("#SignUP").on("click", function () {
                document.getElementById('id01').style.display = 'none'
                document.getElementById('id02').style.display = 'block'
            });

            function _SendImagetoSever(img_src) {
                //var u_id = InfoArray[0].value;
                if (u_id != null && img_src != "") {
                    $.ajax({
                        type: "POST",
                        data: { "uid": u_id, "img_src": img_src, "sub_info": subinfo_type },
                        url: "https://bharatnetprogress.nic.in/POI_API/api/cginfoapi/UpadateImage",
                        contentType: 'application/x-www-form-urlencoded',
                        dataType: "JSON",
                        crossDomain: true,
                        success: function (data, textStatus, jqXHR) {
                            //_OnImageUpload(data);
                            $("#lblMessage").text("Image Updated Successfuly");
                        },
                        error: function (error) {
                            alert(error);
                        }
                    });
                } else {
                    $("#lblMessage").text("Please choose an appropriate image to upload");
                }
            }

            //####################################################### Send Image to DB ###########################################################//
            //######################################################## Add Point to DB ###########################################################//
            $("#AddPointBtn").on("click", _initiatePointAdd);
            $("#btnBacktoGrid").on("click", _initiateBackFn);
            //$("#btnSubmitPoint").on("click", _initiateSubmitPointFn);


            document.getElementById("AddPointFileUpload").addEventListener("change", readFile_AddPoint);

            function readFile_AddPoint() {
                if (this.files && this.files[0]) {
                    if (this.files[0].type == "image/jpeg" && this.files[0].size < 300000) {
                        var FR = new FileReader();
                        FR.addEventListener("load", function (e) {
                            var ContentDiv = document.getElementById("ImageContainer_AddPoint");
                            ContentDiv.innerHTML = "";
                            var data = e.target.result;
                            loadeddata_AddPoint = data.split(",").pop();
                            document.getElementById("img_file").value = loadeddata_AddPoint;
                            $("#img_pre").show();
                            $("#ImageContainer_AddPoint").append("<a target='_blank' href='data:image/jpg;base64," + loadeddata_AddPoint + "' ><img id='myImage_AddPoint' src='data:image/jpg;base64," + loadeddata_AddPoint + "' style=' background-size:100% 100%; max-width:85%;max-height:85%;'/></a>");
                            $("#ImagePreview_AddPoint").hide(); ///changed
                            $("#AddPointMsg").text("Click on Submit to save the previewed Image with the new Point.");
                        });
                        FR.readAsDataURL(this.files[0]);

                        $("#ImageContainer_AddPoint").append("<a target='_blank' href='data:image/jpg;base64," + loadeddata_AddPoint + "' ><img id='myImage_AddPoint' src='data:image/jpg;base64," + loadeddata_AddPoint + "' style=' background-size:100% 100%; max-width:85%;max-height:85%;'/></a>");
                        loadeddata_AddPoint = this.files[0].name;
                        $("#ImagePreview_AddPoint").hide(); ////changed
                        $("#AddPointMsg").text("Click on Submit to save the previewed Image with the new Point.");
                    } else {
                        $("#AddPointMsg").text("The File added is invalid. Please choose a .jpg image of file size less than 300kb");
                    }
                }
            }
            var t_txt, txt, speed;
            function _initiatePointAdd() {
                if (log_Flag) {
                    map.graphics.clear();
                    $("#AddPointTab").slideDown(200);
                    t_txt = 0;
                    txt = "For adding Point Click on the Map";
                    speed = 100;
                    typeWriter();
                }
                else {
                    //alert("Please Login to adding the Point Data")
                    document.getElementById('id01').style.display = 'block';
                }

            }
            function typeWriter() {
                var tytxt = document.getElementById("tytxt").innerHTML;
                if (tytxt.length == 0)
                    t_txt = 0;
                else
                    t_txt = tytxt.length;
                if (t_txt < txt.length) {
                    document.getElementById("tytxt").innerHTML += txt.charAt(t_txt);
                    //t_txt++;
                    setTimeout(typeWriter, speed);
                }
            }
            function _initiateBackFn() {
                //if (Drawtoolbar) {
                //    Drawtoolbar.deactivate();
                //}
                var ContentDiv = document.getElementById("ImageContainer_AddPoint");
                ContentDiv.innerHTML = "";
                map.graphics.clear();
                $("#ImagePreview_AddPoint").hide();
                $("#AddPointTab").slideUp(200);
                $("#LatitudeInput").val("");
                $("#LongitudeInput").val("");
                //$("#selectorInput").empty();
                $("#selectorInput option").filter(function () { return $(this).text() == "Select Facility"; }).prop('selected', true);
                $("#selectorInput2").empty();
                $("#NameInput").val("");
                $("#addressInput").val("");
                $("#desc").val("");
                $("#mobileNoInput").val("");
                $("#AddPointFileUpload").val("");
                $("#AddPointMsg").text("");
                loadeddata_AddPoint = "";
                $("#pnt_info").hide();
                $("#img_pre").hide();
                t_txt = 0;
                document.getElementById("tytxt").innerHTML = "";
                document.getElementById("stnm").innerHTML = "";
                document.getElementById("dtnm").innerHTML = "";
            }



            function _initiateGridReload() {
                // _GetAllDataRequest();
                $("#InfoDialog").hide();
                $("#ImagePreview").hide();
            }
            //######################################################## Add Point to DB ###########################################################//
            //###################################################### Add Point From Map ##########################################################//
            //$("#btnAddPntFrmMap").on("click", _initiatePointAddFromMap);

            function _initiatePointAddFromMap() {
                //if (logFlag) {
                //Drawtoolbar.activate(Draw["POINT"]);
                $("#AddPointMsg").text("Click on Map to add the Point");
                //}
                //else {
                //    //alert("Please Login to add data.");
                //    document.getElementById('id01').style.display = 'block';
                //}
            }

            function addToMap(evt) {
                if (map.getZoom() >= 15) {
                    //fillFacilitiesData();
                    identyfystdtnames(evt.mapPoint);
                    selected_point(evt.mapPoint);
                    //Drawtoolbar.deactivate();

                } else {
                    //$("#AddPointMsg").text("INCREASE MAP ZOOM LEVEL TO ADD POINT");
                    //$("#AddPointMsg").css('background-color', '#abcdef');
                    //$("#AddPointMsg").css('font-size', '20px');
                    //$("#LatitudeInput").prop('disabled', true);
                    //$("#LongitudeInput").prop('disabled', true);
                    //$("#NameInput").prop('disabled', true);
                    alert("Increase map zoom level to add point");
                }
            }
            $("#LongitudeInput").change(function () {
                var pnt_lat = $("#LatitudeInput").val();
                var pnt_long = $("#LongitudeInput").val();
                if (pnt_lat != "" && pnt_long != "") {
                    var point_geometry = webMercatorUtils.geographicToWebMercator(new Point(pnt_long, pnt_lat));
                    identyfystdtnames(point_geometry);
                    selected_point(point_geometry);
                }
                else {
                    alert("Provide Latitude and Longitude Points");
                }
            });
            function selected_point(evt) {
                map.graphics.clear();
                var symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 10, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                    new Color([0, 0, 255]), 3), new Color([0, 255, 255]));
                var graphic = new Graphic(evt, symbol);
                map.graphics.add(graphic);
                var point = new Point(evt);
                var Lat = point.getLatitude().toFixed(4);
                var Long = point.getLongitude().toFixed(4);
                if (evt) {
                    //$("#LatitudeInput").prop('disabled', false);
                    //$("#LongitudeInput").prop('disabled', false);
                    //$("#NameInput").prop('disabled', false);
                    $("#LatitudeInput").val(Lat);
                    $("#LongitudeInput").val(Long);
                    $("#AddPointMsg").text("Point Added, Enter other details and Submit");
                    $("#AddPointMsg").css('font-size', '12px');
                }
                else {
                    $("#LatitudeInput").val(Lat);
                    $("#LongitudeInput").val(Long);
                    $("#AddPointMsg").text("Added Point is out of the selected geometry. \n Please add point in the selected state and district.");
                    $("#AddPointMsg").css('font-size', '12px');
                }
            }
            function identyfystdtnames(evt) {
                // identifyTask = new IdentifyTask(mapServiceUrl + tokenVar);
                identifyTask = new IdentifyTask(vil_service + vil_token);
                identifyParams = new IdentifyParameters();
                identifyParams.tolerance = 3;
                identifyParams.layerIds = [3];
                identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_ALL;
                identifyParams.geometry = evt;
                identifyParams.mapExtent = map.extent;
                identifyTask.execute(identifyParams, function (idResults) {
                    if (idResults.length > 0) {
                        logFlag = true;
                        selected_pointinfo(idResults);
                    }
                    else {
                        identifyParams.layerIds = [2];
                        identifyTask.execute(identifyParams, function (idResults) {
                            if (idResults.length > 0) {
                                logFlag = true;
                                selected_pointinfo(idResults);
                            }
                            else {
                                $("#pnt_info").hide();
                                alert("Selected Point is out of the Range")
                            }
                        });
                    }
                });
            }
            function selected_pointinfo(idResults) {
                stcode = idResults[0].feature.attributes.STCODE11;
                stname = idResults[0].feature.attributes.STNAME;
                dtcode = idResults[0].feature.attributes.DTCODE11;
                dtname = idResults[0].feature.attributes.DTNAME;
                vilcode = idResults[0].feature.attributes.VILCODE11;
                vilname = idResults[0].feature.attributes.VILNAME11;
                $("#st_cd").val(stcode);
                $("#st_nm").val(stname);
                $("#dt_cd").val(dtcode);
                $("#dt_nm").val(dtname);
                $("#pnt_info").show();
                document.getElementById("stnm").innerHTML = "State : " + stname;
                document.getElementById("dtnm").innerHTML = "District : " + dtname;
                if (vilname != " " && vilname != "")
                    document.getElementById("vilnm").innerHTML = "Village : " + vilname;
                else
                    document.getElementById("vilnm").innerHTML = "";
            }
            //State_Count();
            //District_Count();
            function State_Count() {
                $.ajax({
                    type: "GET",
                    url: "https://bharatnetprogress.nic.in/POI_API/api/cginfoapi/GetStateCount",
                    dataType: "JSON",
                    contentType: 'application/x-www-form-urlencoded',
                    crossDomain: true,
                    success: function (dat) {
                        State_MIS_rec = dat;
                        //State_ClusterPoint(State_MIS_rec);
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            }
            function State_ClusterPoint(State_MIS_rec) {
                var st_renderer = new ClassBreaksRenderer(new SimpleMarkerSymbol(), "count");
                st_renderer.setColorInfo({
                    field: "count",
                    minDataValue: 0,
                    maxDataValue: 100,
                    colors: [
                        new Color([243, 217, 201]),
                        new Color([192, 112, 91]),
                        new Color([107, 52, 53])
                    ]
                });
                st_renderer.setSizeInfo({
                    field: "count",
                    minSize: 20,
                    maxSize: 100,
                    minDataValue: 20,
                    maxDataValue: 100
                });
                var st_featureCollection = {
                    "layerDefinition": null,
                    "featureSet": {
                        "features": [],
                        "geometryType": "esriGeometryPoint"
                    }
                };
                st_featureCollection.layerDefinition = {
                    "geometryType": "esriGeometryPoint",
                    "objectIdField": "objectid",
                    "fields": [
                        {
                            "name": "STNAME",//"parameter",
                            "alias": "State",//"Parameter",
                            "type": "esriFieldTypeString"
                        },
                        {
                            "name": "count",
                            "alias": "count",
                            "type": "esriFieldTypeDouble"
                        }
                    ],
                };
                st_featureLayer = new FeatureLayer(st_featureCollection, {
                    id: 'StateCluster'
                });
                map.addLayers([st_featureLayer]);

                var query = new Query();
                query.outFields = ["STCODE11", "STNAME"];
                query.where = "1=1";
                query.returnGeometry = true;
                var queryTask = new QueryTask(vil_service + "/0" + vil_token);
                queryTask.execute(query, function (fs) {
                    featureSet = [];
                    for (var i = 0; i < fs.features.length; i++) {
                        for (var j = 0; j < State_MIS_rec.length; j++) {
                            if (State_MIS_rec[j].state_code == '28')
                                State_MIS_rec[j].state_code = '37';
                            if (Number(fs.features[i].attributes.STCODE11) == Number(State_MIS_rec[j].state_code)) {
                                fs.features[i].attributes.count = Number(State_MIS_rec[j].count);
                                var graphic = new Graphic(fs.features[i].geometry);
                                graphic.setAttributes(fs.features[i].attributes);
                                featureSet.push(graphic);
                                var font = new Font();
                                font.setSize(10);
                                font.setWeight(Font.WEIGHT_BOLD);
                                font.setFamily("Arial Unicode MS");
                                var textSymbol = new TextSymbol(State_MIS_rec[j].count);
                                textSymbol.setColor(new Color([255, 0, 0]))
                                textSymbol.setHaloColor(new Color([255, 255, 255]));
                                textSymbol.setHaloSize(2);
                                textSymbol.setFont(font);
                                textSymbol.setOffset(0, -3);
                                st_count = new Graphic(fs.features[i].geometry, textSymbol, null, null);
                                map.graphics.add(st_count);
                                break;
                            }
                        }
                    }
                    st_featureLayer.applyEdits(featureSet, null, null);
                    st_featureLayer.setRenderer(st_renderer);
                    st_featureLayer.refresh();
                });
            }

            function District_Count() {
                $.ajax({
                    type: "GET",
                    url: "https://bharatnetprogress.nic.in/POI_API/api/cginfoapi/GetDistrictCount",
                    dataType: "JSON",
                    contentType: 'application/x-www-form-urlencoded',
                    crossDomain: true,
                    success: function (dat) {
                        District_MIS_rec = dat;
                        //District_ClusterPoint(District_MIS_rec);
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            }
            function District_ClusterPoint(District_MIS_rec) {
                var dt_renderer = new ClassBreaksRenderer(new SimpleMarkerSymbol(), "count");
                dt_renderer.setColorInfo({
                    field: "count",
                    minDataValue: 0,
                    maxDataValue: 100,
                    colors: [
                        new Color([243, 217, 201]),
                        new Color([192, 112, 91]),
                        new Color([107, 52, 53])
                    ]
                });
                dt_renderer.setSizeInfo({
                    field: "count",
                    minSize: 20,
                    maxSize: 100,
                    minDataValue: 20,
                    maxDataValue: 100
                });
                var dt_featureCollection = {
                    "layerDefinition": null,
                    "featureSet": {
                        "features": [],
                        "geometryType": "esriGeometryPoint"
                    }
                };
                dt_featureCollection.layerDefinition = {
                    "geometryType": "esriGeometryPoint",
                    "objectIdField": "objectid",
                    "fields": [
                        {
                            "name": "dtname",//"parameter",
                            "alias": "District",//"Parameter",
                            "type": "esriFieldTypeString"
                        },
                        {
                            "name": "count",
                            "alias": "count",
                            "type": "esriFieldTypeDouble"
                        }
                    ],
                };
                dt_featureLayer = new FeatureLayer(dt_featureCollection, {
                    id: 'DistrictCluster',
                    showLabels: true
                });
                map.addLayers([dt_featureLayer]);

                var query = new Query();
                query.outFields = ["dtcode11", "dtname"]; 
                query.where = "1=1";
                query.returnGeometry = true;
                var queryTask = new QueryTask(vil_service + "/1" + vil_token);
                queryTask.execute(query, function (fs) {
                    featureSet = [];
                    for (var i = 0; i < fs.features.length; i++) {
                        for (var j = 0; j < District_MIS_rec.length; j++) {
                            if (Number(fs.features[i].attributes.dtcode11) == Number(District_MIS_rec[j].district_code)) {
                                fs.features[i].attributes.count = Number(District_MIS_rec[j].count);
                                var graphic = new Graphic(fs.features[i].geometry);
                                graphic.setAttributes(fs.features[i].attributes);
                                featureSet.push(graphic);
                                var font = new Font();
                                font.setSize(10);
                                font.setWeight(Font.WEIGHT_BOLD);
                                font.setFamily("Arial Unicode MS");
                                var textSymbol = new TextSymbol(District_MIS_rec[j].count);
                                textSymbol.setColor(new Color([255, 0, 0]))
                                textSymbol.setHaloColor(new Color([255, 255, 255]));
                                textSymbol.setHaloSize(2);
                                textSymbol.setFont(font);
                                textSymbol.setOffset(0, -3);
                                //this is the very least of what should be set within the JSON  
                                var json = {
                                    "labelExpressionInfo": { "value": "{count}" },
                                    "labelPlacement": "above-right",
                                    "minScale": 0,
                                };

                                //create instance of LabelClass (note: multiple LabelClasses can be passed in as an array)
                                var labelClass = new LabelClass(json);
                                labelClass.symbol = textSymbol; // symbol also can be set in LabelClass' json
                                //dt_count = new Graphic(fs.features[i].geometry, textSymbol, null, null);
                                //map.graphics.add(dt_count);
                                break;
                            }
                        }
                    }
                    dt_featureLayer.applyEdits(featureSet, null, null);
                    dt_featureLayer.setRenderer(dt_renderer);
                    dt_featureLayer.refresh();
                    //map_zoom();
                });
            }
            map.on("zoom-end", function (evt) {
                if (map.getZoom() >= 15) {
                    $("#AddPointMsg").css('background-color', '#abcdef');
                    $("#AddPointMsg").css('font-size', '20px');
                    $("#AddPointMsg").text("Click on MAP to add point.");
                }
                else $("#AddPointMsg").text("INCREASE MAP ZOOM LEVEL TO ADD POINT");
            });
            function map_zoom() {
                //map.on("zoom-end", function (evt) {
                if (map.getZoom() >= 15) {
                    $("#AddPointMsg").css('background-color', '#abcdef');
                    $("#AddPointMsg").css('font-size', '20px');
                    $("#AddPointMsg").text("Click on MAP to add point.");
                }
                else $("#AddPointMsg").text("INCREASE MAP ZOOM LEVEL TO ADD POINT");
                if (map.getZoom() < 6) {
                    st_featureLayer.show();
                    st_count.show();
                    dt_featureLayer.hide();
                    dt_count.hide();
                    PointsGraphicLayer.hide();
                }
                else if (map.getZoom() < 9) {
                    st_featureLayer.hide();
                    st_count.hide();
                    dt_featureLayer.show();
                    dt_count.show();
                    PointsGraphicLayer.hide();
                }
                else if (map.getZoom() >= 9) {
                    st_featureLayer.hide();
                    st_count.hide();
                    //dt_featureLayer.hide();
                    //dt_count.hide();
                    PointsGraphicLayer.show();
                }
                //});
            }
            //###################################################### Add Point From Map ##########################################################//

        });//End of Require

}

function _initiateSubmitPointFn() {
    //$("#btnSubmitPoint").on("click", function (e) {
    if (logFlag) {
        var lat, long, infoType, subInfoType, infoName, infoAddress, desc, cNo, cMailID;
        lat = $("#LatitudeInput").val();
        long = $("#LongitudeInput").val();
        infoType = $("#selectorInput").val();
        subInfoType = $("#selectorInput2").val();
        infoName = $("#NameInput").val();
        infoAddress = $("#addressInput").val();
        desc = $("#desc").val();
        cNo = $("#mobileNoInput").val();
        cMailID = $("#emailIDInput").val();
        if (lat != "" && long != "" && infoType != "99" && infoName != "" && infoType != "" && subInfoType != "") {
            $.ajax({
                type: "POST",
                data: { "St_Code": stcode, "Dt_Code": dtcode, "User_Nm": email, "Latitude": lat, "Longitude": long, "InfoType": infoType, "SubInfoType": subInfoType, "Info_Nm": infoName, "Info_Add": infoAddress, "Description": desc, "St_Name": stname, "Dt_Name": dtname, "ContactNo": cNo, "ContactMailID": email, "Img_source": loadeddata_AddPoint, "Img_Submission": "" },
                //url: "WebService.asmx/InsertPointToDB",
                url: "https://bharatnetprogress.nic.in/POI_API/api/cginfoapi/SubmissionData",
                contentType: 'application/x-www-form-urlencoded',
                dataType: "text",
                //headers: csrf_token,
                crossDomain: true,
                async: false,
                success: function (data, textStatus, jqXHR) {
                    Get_All(email, "");
                },
                error: function (error) {
                    alert(error);
                    dom.byId("bodyloadimg").style.display = "none";
                }
            });
        } else {
            $("#AddPointMsg").text("Please input all mandatory values.");
            dom.byId("bodyloadimg").style.display = "none";
        }
    } else {
        document.getElementById('id01').style.display = 'block';
    }
    //});
}
function imagepre() {
    var url = 'data:image/jpg;base64,' + document.getElementById('img_file').value;
    img = '<img src="' + url + '">';
    popup = window.open(url, 'Image', 'width=562, height=487, scrollbars=1,resizable=1');
    popup.document.write(img);
    //window.open(url, 'Image', 'width=562, height=487, scrollbars=1,resizable=1');
    //window.open(this.href, 'popUpWindow', 'width=562, height=487, scrollbars=1').focus()
}
function submit_values() {
    var lat, long, infoType, subInfoType, infoName, infoAddress, desc, cNo, cMailID;
    lat = $("#LatitudeInput").val();
    long = $("#LongitudeInput").val();
    infoType = $("#selectorInput").val();
    if (infoType == "Fair Price Shop" || infoType == "Tourist Places" || infoType == "Heritage Spots / Monuments") {
        subInfoType = "";
    }
    else {
        subInfoType = $("#selectorInput2").val();
    }
    infoName = $("#NameInput").val();
    uploaded_img = document.getElementById("AddPointFileUpload");
    if (lat != "" && long != "" && infoName != "" && infoType != "Select" && subInfoType != "Select") {
        if (uploaded_img.files.length > 0) {
            if (uploaded_img.files[0].type == "image/jpeg" && uploaded_img.files[0].size < 300000) {
                if (confirm("Conform To Submit the Data")) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert("Please Upload the Image below 300kb and jpg/jpeg type only");
                return false;
            }
        }
        else {
            if (confirm("Conform To Submit the Data")) {
                return true;
            }
            else {
                return false;
            }
        }

    }
    else {
        alert("Please Enter All mandatory Fields");
        return false;
    }
}
var name, id, email, imgurl;

function inputvalidations(random) {
    var uname = $("#uname").val();
    var pwd = $("#pwd").val();
    var captch = $('#txt_Code').val();
    var ranSalt = random;
    if (uname == null || uname == "") {
        alert("Please Enter User Name");
        return false;
    }
    else if (pwd == null || pwd == "") {
        alert("Please Enter Password");
        return false;
    }
    else if (captch == null || captch == "") {
        alert("Please Enter Captcha");
        return false;
    }
    else {
        document.getElementById("hash").value = sha256(ranSalt + sha256(pwd));
        document.getElementById("pwd").value = sha256(ranSalt + sha256(pwd));
        return true;
    }
}

function LogUser(uname) {
    $("#changepwdiv").show();
    $("#wel_txt").show();
    $("#user_type").val("l_user");
    log_Flag = true;
    //$("#wel_txt").val("Welcome to " + uname);
    document.getElementById("wel_txt").innerHTML = "Welcome to " + uname;
    email = uname;
    Get_All(uname, "");
}
function SignUser() {
    var sUsername = $("#uname1").val();
    if (sUsername.match(/^[A-Za-z0-9_]{6,15}$/)) {
        var sPwd1 = $("#pwd1").val(); var sPwd2 = $("#pwd2").val();
        if (sPwd1 != "" && sPwd2 != "") {
            sPwd1 = sha256(sPwd1); sPwd2 = sha256(sPwd2);
            if (sPwd1 == sPwd2) {
                var sMobile = $("#mobNo").val();
                if (sMobile.match(/^[0-9]{10,12}$/)) {
                    var email = $("#emailID").val();
                    if (email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
                        var sQNo = $("#sQuestion").val();
                        var sQAns = $("#sQAns").val();
                        if (sQNo != "99" && sQAns != "") {
                            sQAns = sha256(sQAns);
                            document.getElementById("sQAns").value = sQAns;
                            var ranSalt = $('#Btn_Login').val();
                            document.getElementById("hash").value = sPwd1;//sha256(ranSalt + sha256(sPwd1));
                            document.getElementById("pwd1").value = document.getElementById("pwd2").value = "";
                            if ($('#txt_Code1').val() != "") {
                                return true;
                            }
                            else {
                                alert("Please Enter Captcha");
                                return false;
                            }
                        }
                        else {
                            alert("Choose question and give answer.");
                            return false;
                        }
                    }
                    else {
                        alert("Please Provide Valid Email Address.");
                        return false;
                    }
                }
                else {
                    alert("Please Provide Valid Mobile Number.");
                    return false;
                }
            }
            else {
                alert("Passwords are not matching.");
                return false;
            }
        }
        else {
            alert("Password should not be empty.");
            return false;
        }
    }
    else {
        alert("Username should be minimum of 6 characters.");
        return false;
    }
}

function CheckUser() {
    var sUsername = $("#uname1").val();
    if (sUsername != "" && sUsername.length >= 6) {
        $.ajax({
            type: "GET",
            //data: { "Username": sUsername},
            //url: "WebService.asmx/CheckUser",
            url: "https://bharatnetprogress.nic.in/POI_API/api/cginfoapi/CheckUser?Username=" + sUsername,
            dataType: "JSON",
            contentType: 'application/x-www-form-urlencoded',
            crossDomain: true,
            success: function (dat) {

                //dat.getElementsByTagName("string")["0"].childNodes["0"].data == "Success" ? document.getElementById('chkUsr').style.display = 'none' : document.getElementById('chkUsr').style.display = 'block';;
                //document.getElementById('chkUsr').innerHTML = dat.getElementsByTagName("string")["0"].childNodes["0"].data;
                if (dat == "Success")
                    document.getElementById('chkUsr').style.color = "#187403";
                else
                    document.getElementById('chkUsr').style.color = "red";
                document.getElementById('chkUsr').innerHTML = dat;
                document.getElementById('chkUsr').style.display = 'block';
            },
            error: function (error) {
                alert(error);
            }
        });
    } else {
        document.getElementById('chkUsr').innerHTML = "Username should be minimum of 6 characters.";
        document.getElementById('chkUsr').style.color = "red";
        document.getElementById('chkUsr').style.display = 'block';
    }
}

function statusChangeCallback(response, flag) {
    //var name, id, email, imgurl;


    if (flag == 0) {
        $("#s_username").val(response.email);
        console.log(response);
        name = response.name;
        id = response.id;
        if (response.email == undefined) {
            alert("Please link your mail id to facebook account.");
            return;
        } else email = response.email;

        //email = response.email == undefined ? "email is not available" : response.email;
        imgurl = response.picture.data.url;
    }
    else if (flag == 1) {
        $("#s_username").val(response.getEmail());
        name = response.getName();
        id = response.getId();
        email = response.getEmail();
        imgurl = response.getImageUrl();
    }
    document.getElementById("btnSample").click();
    $.ajax({
        type: "POST",
        data: { "UserName": name, "U_Id": id, "Email_Id": email, "Img_Url": imgurl, "Flag": "1" },
        //url: "WebService.asmx/LoginUser",
        url: "https://bharatnetprogress.nic.in/POI_API/api/cginfoapi/LoginUser",
        //contentType: 'application/x-www-form-urlencoded',
        dataType: "JSON",
        crossDomain: true,
        success: function (dat) {
            //console.log(dat);
            //if (dat.children["0"].textContent == "Success") {
            log_Flag = logFlag = true;
            //User_Name = response.getName();
            User_Name = name;
            $("#user_type").val("s_user");
            $("#wel_txt").show();
            document.getElementById("wel_txt").innerHTML = "Welcome to " + User_Name;
            document.getElementById("log1").style.display = "none";
            document.getElementById("id01").style.display = "none";
            document.getElementById("logout_image").src = imgurl;
            document.getElementById("logout_image").title = name;
            document.getElementById("log2").style.display = "block";
            //location.reload(true);
            // _GetAllDataRequest(name);
            Get_All(email, "S_User");
            // }
        },
        error: function (error) {
            alert(error.message);
        }
    });
}


function TestScript() {
    //document.getElementById('id01').style.display = 'none';
    //alert("check ..");
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
    console.log("logging out");
}
function bigImg(x) {
    //x.style.height = "64px";
    //x.style.width = "64px";
    $('#grid_img_div').show();
    document.getElementById("grid_img").src = x.src;
}

function normalImg(x) {
    //x.style.height = "32px";
    //x.style.width = "32px";
    $('#grid_img_div').hide();
}
function dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById(elmnt.id + "Header")) {
        /* if present, the header is where you move the DIV from:*/
        document.getElementById(elmnt.id + "Header").onmousedown = dragMouseDown;
    } else {
        /* otherwise, move the DIV from anywhere inside the DIV:*/
        elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        /* stop moving when mouse button is released:*/
        document.onmouseup = null;
        document.onmousemove = null;
    }
}