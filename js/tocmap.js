 var map;

    require(["esri/map", 
            "esri/config",  
             "esri/geometry/Extent",  
             "esri/layers/ArcGISDynamicMapServiceLayer",  
             "esri/layers/FeatureLayer",  
             "esri/renderers/ClassBreaksRenderer",  
             "esri/symbols/SimpleFillSymbol",  
             "esri/tasks/GeometryService",  
             "dojo/dom",  
             "dojo/dom-geometry",  
             "dojo/fx",  
             "dojo/on",  
             "dojo/parser",  
             "dojo/_base/Color",  
             "agsjs/dijit/TOC",  
             "dojo/_base/array",
             "dojo/domReady!"], function (
                 Map, esriConfig, Extent, ArcGISDynamicMapServiceLayer, 
                 FeatureLayer, ClassBreaksRenderer, SimpleFillSymbol, GeometryService,
                dom, domGeom, coreFx, on, parser, Color
              , TOC  ,arrayUtils
    ) {  

            map = new Map("mapDiv", {
             center: [78, 24],
             zoom: 5,
             basemap: "streets",
             slider: false,

         });

                operationalLayer = new ArcGISDynamicMapServiceLayer(serviceLayer);  

                map.addLayers([operationalLayer]);  

                 // Add Table of Contents Start  
                 map.on('layers-add-result', function (evt) {  
                     try {  
					 var toc_layerInfos = arrayUtils.map(evt.layers, function (layer, index) {
						return { layer: layer.layer,  collapsed: true, slider: false };
					});
                         var toc = new TOC({  
                             map: map,  
                             layerInfos: toc_layerInfos
                         }, "tocDiv");  
                         toc.startup();  

                         toc.on("load", function () {  
                             console.log("TOC loaded");  
                         });   
                     }  
                     catch (e) { console.error(e.message); }  
                 });  
             }  
    );  