﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
public partial class Captcha : System.Web.UI.Page
{
    public string captcha_Check = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Drawing.Bitmap b = new Bitmap(140, 35);
        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(b);
        g.Clear(System.Drawing.Color.White);
        g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
        System.Drawing.Font f = new Font("Verdana", 18, System.Drawing.FontStyle.Bold);
        string strrandom = "";
        string[] strArray = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
        string[] strArray1 = new string[] { "2", "3", "4", "5", "6", "7", "8", "9" };
        string[] strArray2 = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        Random rd = new Random();
        int x;
        string[] strArray3 = new string[] { "0", "1", "2", "3", "4", "5" };
        int cnum = Convert.ToInt32(rd.Next(0, 5));
        int cnum1 = Convert.ToInt32(strArray3[cnum]);
        for (x = 0; x < 6; x++)
        {
            if (x % 2 == 0)
            {
                if (x != cnum1)
                {
                    int i = Convert.ToInt32(rd.Next(0, 23));
                    strrandom += strArray[i].ToString();
                }
                else
                {
                    int i1 = Convert.ToInt32(rd.Next(0, 23));
                    strrandom += strArray2[i1].ToString();
                }
            }
            else
            {
                if (x != cnum1)
                {
                    int i = Convert.ToInt32(rd.Next(0, 7));
                    strrandom += strArray1[i].ToString();
                }
                else
                {
                    int i1 = Convert.ToInt32(rd.Next(0, 23));
                    strrandom += strArray2[i1].ToString();
                }
            }
        }
        Session["CAPTCHA"] = null;
        Session.Add("CAPTCHA", strrandom);
        string script = string.Format("var captcha_Check = '{0}'; ", strrandom);
        captcha_Check = strrandom;
        if (!ClientScript.IsClientScriptBlockRegistered("Configuration"))
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "Configuration", script, true);
        }
        g.DrawString(strrandom, f, System.Drawing.Brushes.Black, 3, 3);
        Response.ContentType = "image/GIF";
        b.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Gif);
        f.Dispose();
        g.Dispose();
        b.Dispose();
    }
}